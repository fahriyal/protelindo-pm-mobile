package com.gz.databaselibrary;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Rio Rizky Rainey
 */
public class PostBackgroundService extends Service {

    ArrayList<AsyncProcess> list_ap;

    @Override
    public int onStartCommand(Intent intent, int flag, int startId) {
        List<Queue> listQueue = Queue.toArrayList(Queue.class);
        for (Queue queue : listQueue) {
            new AsyncProcess(queue).execute();
        }
        stopSelf();
        return super.onStartCommand(intent, flag, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public class AsyncProcess extends AsyncTask<String, String, String> {

        private Queue queue;

        public AsyncProcess(Queue queue) {
            this.queue = queue;
        }

        @Override
        protected String doInBackground(String... params) {
            List<QueueParamater> qParamaterList = QueueParamater.getQueueParameterByIdQueue(queue.getIdQueue().toString());
            ServiceHandler sHandler = new ServiceHandler(getBaseContext());
            for (QueueParamater queueParamater : qParamaterList) {
                sHandler.addParam(queueParamater.getName(), queueParamater.getValue());
            }
            Log.d("Upload data from local", sHandler.getParamsList().toString());
            return sHandler.makeServiceCallPostBackground(queue.getAction(), ServiceHandler.POST);
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject != null) {
                    if (jsonObject.optInt("flag") == 1) {
                        QueueParamater.delete(QueueParamater.class, "id_queue=?", new String[]{queue.getIdQueue().toString()});
                        Queue.delete(Queue.class, "id_queue=?", new String[]{queue.getIdQueue().toString()});
                    }
                }
            } catch (Exception e) {

            }
        }

    }

}