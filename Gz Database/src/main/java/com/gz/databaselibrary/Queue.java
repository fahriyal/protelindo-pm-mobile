package com.gz.databaselibrary;

import com.gz.databaselibrary.annotation.Column;
import com.gz.databaselibrary.annotation.Id;
import com.gz.databaselibrary.annotation.Table;

@Table(name = "queue")
public class Queue extends GzEntity {

	/**
	 * VersionUID
	 */
	private static final long serialVersionUID = -2794012041107516509L;

	@Id
	@Column(name = "id_queue")
	private Integer idQueue;
	
	@Column(name = "action")
	private String action;
	
	@Column(name = "status")
	private Integer status;

	public Queue() {}

	public Queue(String action, Integer status) {
		this.action = action;
		this.status = status;
	}

	public Integer getIdQueue() {
		return idQueue;
	}

	public void setIdQueue(Integer idQueue) {
		this.idQueue = idQueue;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
