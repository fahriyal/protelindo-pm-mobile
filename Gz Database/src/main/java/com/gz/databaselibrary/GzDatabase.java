package com.gz.databaselibrary;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;

public class GzDatabase extends SQLiteOpenHelper {

    public static String name;
    public static int version;
    public static boolean isOnlineMode;
    protected static SQLiteDatabase database;
    private static GzDatabase main;
    private Context context;

    public GzDatabase(Context context) {
        super(context, name, null, version);
        this.context = context;
    }

    public static GzDatabase getInstance(Context context) {
        if (main == null) {
            main = new GzDatabase(context);
        }
        return main;
    }

    public static GzDatabase getInstance() {
        if (main != null) {
            return main;
        }
        return null;
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        for (int i = 1; i <= version; i++) {
            String[] query = extractQuery(i + "");
            for (String aQuery : query) {
                database.execSQL(aQuery + ";");
            }
        }

        if (isOnlineMode) {
            database.execSQL(TableOnline.QUEUE);
            database.execSQL(TableOnline.QUEUE_PARAM);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion,
                          int newVersion) {
        Log.d("old version database", "" + oldVersion);
        Log.d("new version database", "" + newVersion);
        for (int i = oldVersion + 1; i <= newVersion; i++) {
            String[] query = extractQuery(i + "");
            Log.d("database ", "" + query.length);
            for (String aQuery : query) {
                database.execSQL(aQuery + ";");
            }
        }
    }

    public SQLiteDatabase getDatabase() {
        return database;
    }

    public void open() throws SQLException {
        database = getWritableDatabase();
    }

    public void close() {
        database.close();
    }

    public long insert(String table, ContentValues cv) {
        return database.insert(table, null, cv);
    }

    public long update(String table, ContentValues cv, String whereClause,
                       String[] whereArgs) {
        return database.update(table, cv, whereClause, whereArgs);
    }

    public long delete(String table, String whereClause, String whereArgs[]) {
        return database.delete(table, whereClause, whereArgs);
    }

    public long deleteTableData(String table) {
        return database.delete(table, null, null);
    }

    /**
     * Extract query from specified file in the assets folder.
     *
     * @param fileName Name of file to be extracted.
     * @return Extracted query.
     */
    private String[] extractQuery(String fileName) {
        String[] returnedValue = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            int size = is.available();

            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            String text = new String(buffer);

            returnedValue = text.split(";");

        } catch (IOException e) {
            e.printStackTrace();
        }
        return returnedValue;
    }

    /**
     * Delete all data in every table of database
     */
    public void deleteAllData() {
        Cursor cursor = database.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
        String tableName[] = new String[cursor.getCount()];
        int i = 0;
        while (cursor.moveToNext()) {
            tableName[i++] = cursor.getString(0);
        }
        cursor.close();
        for (String aTableName : tableName) {
            if (!aTableName.equalsIgnoreCase("queue") && !aTableName.equalsIgnoreCase("queue_parameter"))
                deleteTableData(aTableName);
        }
    }

}
