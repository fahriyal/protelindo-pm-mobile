package com.gz.databaselibrary;

import android.database.Cursor;

import com.gz.databaselibrary.annotation.Column;
import com.gz.databaselibrary.annotation.Id;
import com.gz.databaselibrary.annotation.Table;

import java.util.ArrayList;
import java.util.List;

@Table(name = "queue_parameter")
public class QueueParamater extends GzEntity {

	/**
	 * VersionUID
	 */
	private static final long serialVersionUID = 8150422902139717310L;

	@Id
	@Column(name = "id_queue_parameter")
	private Integer idQueueParameter;

	@Column(name = "id_queue")
	private Integer idQueue;

	@Column(name = "name")
	private String name;

	@Column(name = "value")
	private String value;

	public QueueParamater() {
	}

	public QueueParamater(Integer idQueue, String name, String value) {
		this.idQueue = idQueue;
		this.name = name;
		this.value = value;
	}

	public static List<QueueParamater> getQueueParameterByIdQueue(String idQueue) {
		List<QueueParamater> list = new ArrayList<QueueParamater>();
		String sql = "SELECT * FROM queue_parameter WHERE id_queue=?";
		Cursor cursor = GzDatabase.getInstance().getDatabase().rawQuery(sql, new String[]{idQueue});
		while (cursor.moveToNext()) {
			QueueParamater entity = (QueueParamater) Utils.setEntityFromCursor(QueueParamater.class, cursor);
			list.add(entity);
		}
		return list;
	}

	public Integer getIdQueueParameter() {
		return idQueueParameter;
	}

	public void setIdQueueParameter(Integer idQueueParameter) {
		this.idQueueParameter = idQueueParameter;
	}

	public Integer getIdQueue() {
		return idQueue;
	}

	public void setIdQueue(Integer idQueue) {
		this.idQueue = idQueue;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
