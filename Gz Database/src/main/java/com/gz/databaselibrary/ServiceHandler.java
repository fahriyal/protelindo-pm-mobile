package com.gz.databaselibrary;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class ServiceHandler implements AsyncTaskListener {
    public final static int GET = 1;
    public final static int POST = 2;
    private static String url;
    private String response = null;
    private ConnectionListener connectionListener;
    private Context context;
    private String message = "Loading...";
    private boolean showProgressDialog = true;
    private List<NameValuePair> paramsList;

    private ConnectionAsyncTask connectionAsyncTask;

    public ServiceHandler(Context context) {
        this.context = context;
        paramsList = new ArrayList<NameValuePair>();
        addParam("APIKEY", "2D4FC5AC43BFB5B46E722EF762E24");
    }

    public ServiceHandler(Context context, boolean showProgressDialog) {
        this.context = context;
        this.showProgressDialog = showProgressDialog;
        paramsList = new ArrayList<NameValuePair>();
        addParam("APIKEY", "2D4FC5AC43BFB5B46E722EF762E24");
    }

    public ServiceHandler(Context context, String message) {
        this.context = context;
        this.message = message;
        paramsList = new ArrayList<NameValuePair>();
        addParam("APIKEY", "2D4FC5AC43BFB5B46E722EF762E24");
    }

    public ServiceHandler(Context context, String message, boolean showProgressDialog) {
        this.context = context;
        this.message = message;
        this.showProgressDialog = showProgressDialog;
        paramsList = new ArrayList<NameValuePair>();
        addParam("APIKEY", "2D4FC5AC43BFB5B46E722EF762E24");
    }

    public void setConnectionListener(ConnectionListener connectionListener) {
        this.connectionListener = connectionListener;
    }

    public List<NameValuePair> getParamsList() {
        return paramsList;
    }

    public void destroyAsync() {
        if (connectionAsyncTask != null)
            connectionAsyncTask.cancel(true);
    }

    /**
     * Making service call
     *
     * @url - url to make request
     * @method - http request method
     */
    public String makeServiceCallAsyncTask(String url, int method) {
        System.out.println("URL: "+url);
        return this.makeServiceCallAsyncTask(url, method, paramsList);
    }

    public String makeServiceCallPostBackground(String url, int method) {
        return makeServiceCall(url, method, paramsList);
    }

    public String makeServiceCall(String url, int method) {
        return makeServiceCall(url, method, paramsList);
    }

    public String makeServiceCall(String url, int method, List<NameValuePair> params) {
        try {
            HttpParams httpParameters = new BasicHttpParams();
//            // Set the timeout in milliseconds until a connection is established.
//            // The default value is zero, that means the timeout is not used.
//            int timeoutConnection = 30000;
//            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
//            // Set the default socket timeout (SO_TIMEOUT)
//            // in milliseconds which is the timeout for waiting for data.
//            int timeoutSocket = 30000;
//            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            // http client
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
            HttpEntity httpEntity = null;
            HttpResponse httpResponse = null;

            for (NameValuePair nameValuePair : params) {
                System.out.println("akses data: "+nameValuePair.getName()+" -- value: " + nameValuePair.getValue());
            }
            System.out.println("akses data: "+url);

            // Checking http request method type
            if (method == POST) {
                HttpPost httpPost = new HttpPost(url);
                // adding post params
                if (params != null) {
                    httpPost.setEntity(new UrlEncodedFormEntity(params));
                }

                httpResponse = httpClient.execute(httpPost);

            } else if (method == GET) {
                // appending params to url
                if (params != null) {
                    String paramString = URLEncodedUtils.format(params, "utf-8");
                    url += "?" + paramString;
                }
                HttpGet httpGet = new HttpGet(url);
                ServiceHandler.url = url;

                httpResponse = httpClient.execute(httpGet);

            }
            httpEntity = httpResponse.getEntity();
            response = EntityUtils.toString(httpEntity);
            System.out.println("akses data response: " + response);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String makeServiceCallWithConflict(String url, int method) {
        return makeServiceCallWithConflict(url, method, paramsList);
    }

    /**
     * Making service call with conflict when device not connected to internet
     *
     * @url - url to make request
     * @method - http request method
     * @params - http request params
     */
    public String makeServiceCallWithConflict(String url, int method, List<NameValuePair> params) {
        Log.d("isOnline", "" + Utils.isOnLine(context));
        if (Utils.isOnLine(context)) {
            return makeServiceCallAsyncTask(url, method, params);
        } else {
            Queue queue = new Queue(url, 0);
            queue.save();
            for (NameValuePair nameValuePair : params) {
                QueueParamater qParamater = new QueueParamater(queue.getIdQueue(), nameValuePair.getName(),
                        nameValuePair.getValue());
                qParamater.save();
            }
            return null;
        }
    }

    public String makeServiceCallWithConflictNoAsync(String url, int method) {
        return makeServiceCallWithConflictNoAsync(url, method, paramsList);
    }

    public String makeServiceCallWithConflictNoAsync(String url, int method, List<NameValuePair> params) {
        if (Utils.isOnLine(context)) {
            return makeServiceCall(url, method, params);
        } else {
            Queue queue = new Queue(url, 0);
            queue.save();
            for (NameValuePair nameValuePair : params) {
                QueueParamater qParamater = new QueueParamater(queue.getIdQueue(), nameValuePair.getName(),
                        nameValuePair.getValue());
                qParamater.save();
            }
            return null;
        }
    }

    public String makeServiceCallAsyncTask(String url, int method, List<NameValuePair> params) {
        final int finalMethod = method;
        final List<NameValuePair> finalParams = params;
        connectionAsyncTask = new ConnectionAsyncTask(context, message, showProgressDialog) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if (connectionListener != null)
                    connectionListener.onBeforeRequest();
                for (NameValuePair nameValuePair : finalParams) {
                    Log.d("param: " + nameValuePair.getName(), "value: " + nameValuePair.getValue());
                }
            }

            @Override
            protected String doInBackground(String... params) {
                return makeServiceCall(params[0], finalMethod, finalParams);
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                System.out.println("response: " + result);
                if (connectionListener != null && !TextUtils.isEmpty(result))
                    connectionListener.onAfterRequested(result);
//                if(TextUtils.isEmpty(result))
//                    Toast.makeText(context, "Terjadi kesalahan dalam menghubungi server", Toast.LENGTH_LONG).show();
                sendResult(result);
            }
        };
        connectionAsyncTask.setListener(this);
        connectionAsyncTask.execute(url);
        return response;
    }

    public void addParam(String key, String value) {
        paramsList.add(new BasicNameValuePair(key, value));
    }

    public String getUrl() {
        return url;
    }

    public String getResponse() {
        return response;
    }

    public String uploadImageNoAsync(String url, String key, String path) {
        String response_str = "";

        HttpParams httpParameters = new BasicHttpParams();
//        int timeoutConnection = 25000;
//        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
//        int timeoutSocket = 25000;
//        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

        HttpClient httpClient = new DefaultHttpClient(httpParameters);
        HttpPost post = new HttpPost(url);
//        post.addHeader("Cache-Control", "no-cache");
        MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

        InputStream is = null;

        try {
            for (int i = 0; i < paramsList.size(); i++) {
                NameValuePair valuePair = paramsList.get(i);
                String value = valuePair.getValue();
                if (value == null) value = "";
                reqEntity.addPart(valuePair.getName(), new StringBody(value));
            }
            if (!TextUtils.isEmpty(path))
                reqEntity.addPart(key, new FileBody(new File(path)));

            post.setEntity(reqEntity);

            try {
                HttpResponse response = httpClient.execute(post);
                HttpEntity resEntity = response.getEntity();
                is = resEntity.getContent();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }

        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
            return null;
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            response_str = sb.toString();
            //Log.e("response upload avatar", response_str);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
            //Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        return response_str;
    }

    public String uploadImage(String url, String key, String path) {
        final String urlFinal = url;
        final int finalMethod = POST;
        final String keyFinal = key;
        final String pathFinal = path;
        final List<NameValuePair> finalParams = paramsList;
        connectionAsyncTask = new ConnectionAsyncTask(context, message, showProgressDialog) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if (connectionListener != null)
                    connectionListener.onBeforeRequest();
                for (NameValuePair nameValuePair : finalParams) {
                    Log.d("param: " + nameValuePair.getName(), "value: " + nameValuePair.getValue());
                }
            }

            @Override
            protected String doInBackground(String... params) {
                return uploadImageNoAsync(urlFinal, keyFinal, pathFinal);
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                Log.d("result", "" + result);
                if (connectionListener != null)
                    connectionListener.onAfterRequested(result);
                sendResult(result);
            }
        };
        connectionAsyncTask.setListener(this);
        connectionAsyncTask.execute();
        return response;
    }

    @Override
    public void sendResult(String data) {
        response = data;
    }


    private abstract class ConnectionAsyncTask extends AsyncTask<String, String, String> {

        private Context context;
        private String message = "Loading...";
        private boolean showProgressDialog = true;
        private ProgressDialog progressDialog;

        private AsyncTaskListener listener;

        /**
         * @param context
         * @param message
         * @param showProgressDialog
         */
        public ConnectionAsyncTask(Context context, String message, boolean showProgressDialog) {
            this.context = context;
            this.message = message;
            this.showProgressDialog = showProgressDialog;
            if (showProgressDialog) {
                initProgressDialog();
            }
        }

        public void setListener(AsyncTaskListener listener) {
            this.listener = listener;
        }

        private void initProgressDialog() {
            progressDialog = new ProgressDialog(context);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);
            progressDialog.setMessage(message);
        }

        @Override
        protected void onPreExecute() {
            if (showProgressDialog) {
                progressDialog.show();
            }
        }

        @Override
        protected abstract String doInBackground(String... params);

        @Override
        protected void onPostExecute(String result) {
            response = result;
            Log.d("response: ", "" + result);
            listener.sendResult(result);
            if (showProgressDialog) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        }
    }

}
