package id.co.firzil.protelindopmmobile;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;

import com.google.firebase.iid.FirebaseInstanceId;

import java.io.File;

import id.co.firzil.protelindopmmobile.ui.TextProgressBar;

public class SplashActivity extends Activity implements View.OnClickListener {

    boolean exit = false;
    private Context context;
    private int press = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String regid = FirebaseInstanceId.getInstance().getToken();
        if(regid == null) regid = "";

        System.out.println("splash regid = "+regid);

        if(! regid.equals(Me.getValue(this, Me.FCM_REGID))){
            Me.commit(this, Me.FCM_REGID, regid);
            new FcmPreference(this).setIsRegisteredInServer(false);
        }

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_splash);
        SessionVersionChecker me = new SessionVersionChecker(this);
        me.setIsSudahNgeCekVersi(false);

        context = this;

//        Me.commit(context, "radius_tolerance", "200");
//        Me.commit(context, "min_distance_update_gps", "1");
//        Me.commit(context, "min_time_update_gps", "3000");

        View secretButton = findViewById(R.id.button_secret);
        secretButton.setOnClickListener(this);
        URL.URL_SERVER = getSharedPreferences("id.co.firzil.protelindopmmobile", MODE_PRIVATE).getString("URL_SERVER", "https://pmcm.protelindo.net:6443/");
        URL.changeAllURL();
        Log.d("URL_SERVER", URL.URL_SERVER);

        int enabledAutoTime = Utils.isEnabledAutoTime(this);
        int enableAutoTimeZone = Utils.isEnabledAutoTimeZone(this);

        if (enabledAutoTime == 0 || enableAutoTimeZone == 0) {
            int idTitleDialog = R.string.TimeAlertDialogTitle;
            int idMessageDialog = R.string.TimeAlertDialogMessage;

            final Intent intentSetting = new Intent(Settings.ACTION_DATE_SETTINGS);

            new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setTitle(idTitleDialog)
                    .setMessage(idMessageDialog)
                    .setPositiveButton(R.string.settings, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            context.startActivity(intentSetting);
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            finish();
                        }
                    })
                    .show();

        }
        else {
            TextProgressBar textProgressBar = (TextProgressBar) findViewById(R.id.progressBar);

            textProgressBar.setTextColor(getResources().getColor(android.R.color.black));
            textProgressBar.setMax(100);
            textProgressBar.setText("0%");

            new LoadingTask(context, textProgressBar).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    @Override
    public void onClick(View v) {
        press++;
        press = press % 5;
        if (press == 0) {
            exit = true;
            startActivity(new Intent(this, UrlServerActivity.class));
            finish();
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */

    public class LoadingTask extends AsyncTask<String, Integer, Integer> {

        // This is the progress bar you want to update while the task is in progress
        private final TextProgressBar progressBar;

        private Context context;

        public LoadingTask(Context context, TextProgressBar progressBar) {
            this.context = context;
            this.progressBar = progressBar;
        }

        @Override
        protected Integer doInBackground(String... params) {

            int progress = (int) ((0.2 / (float) 3) * 100);
            try {
                Thread.sleep(1500);
            } catch (InterruptedException ignore) {
            }
            publishProgress(progress);

            File fileCamera = new File(Constants.FILE_IMAGE);
            if (!fileCamera.exists()) {
                fileCamera.mkdirs();
            }

            try {
                Thread.sleep(1500);
            } catch (InterruptedException ignore) {
            }
            progress = (int) ((1 / (float) 3) * 100);
            publishProgress(progress);

            try {
                new UpdateRegid(SplashActivity.this).updateSync();
                Thread.sleep(500);
            } catch (InterruptedException ignore) {
            }
            progress = (int) ((2 / (float) 3) * 100);
            publishProgress(progress);

            try {
                Thread.sleep(1500);
            } catch (InterruptedException ignore) {
            }
            progress = (int) ((9 / (float) 10) * 100);
            publishProgress(progress);
            // Perhaps you want to return something to your post execute
            return 1234;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            progressBar.setProgress(values[0]);
            progressBar.setText(values[0] + "%");
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if (! exit) {
                if (Me.isLogin(context)) startActivity(new Intent(context, DashboardActivity.class));
                else
                    startActivity(new Intent(context, LoginActivity.class));
                finish();
            }
        }
    }

    public void onBackPressed(){
        super.onBackPressed();
        exit = true;
    }

}
