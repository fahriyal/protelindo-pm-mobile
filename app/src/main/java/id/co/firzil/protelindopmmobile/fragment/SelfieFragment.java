package id.co.firzil.protelindopmmobile.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.File;

import id.co.firzil.protelindopmmobile.Constants;
import id.co.firzil.protelindopmmobile.CustomImageLoader;
import id.co.firzil.protelindopmmobile.Delivery;
import id.co.firzil.protelindopmmobile.ImageCompressionAsyncTask;
import id.co.firzil.protelindopmmobile.R;
import id.co.firzil.protelindopmmobile.Tag;
import id.co.firzil.protelindopmmobile.Utils;
import id.co.firzil.protelindopmmobile.entity.Meta;
import id.co.firzil.protelindopmmobile.entity.TaskList;
import id.co.firzil.protelindopmmobile.ui.Alert;

public class SelfieFragment extends BaseActivity implements View.OnClickListener {

    private Uri tempUri, selectedUri;
    private ImageView imageSelfie;
    private TaskList taskList;
    private CustomImageLoader imageLoader;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        taskList = (TaskList) getIntent().getExtras().getSerializable("taskList");
        if(savedInstanceState != null){
            taskList = (TaskList) savedInstanceState.getSerializable("taskList");
        }

        setContentView(R.layout.fragment_selfie);
        setNavbar();
        String TAG = "Please take a selfie";
        setTitle(TAG);
        imageLoader = new CustomImageLoader(SelfieFragment.this);
        ImageView imageCamera = (ImageView) findViewById(R.id.image_camera);
        imageSelfie = (ImageView) findViewById(R.id.image_selfie);

        imageCamera.setOnClickListener(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try{
            if (resultCode == Activity.RESULT_OK && requestCode == Tag.REQUEST_CODE_CAMERA) {
                selectedUri = tempUri;
                ImageCompressionAsyncTask imageCompressionAsyncTask = new ImageCompressionAsyncTask(c, null) {
                    @Override
                    protected void onPostExecute(String result) {
                        super.onPostExecute(result);
                        imageLoader.displayImage("file://" + selectedUri.getPath(), imageSelfie);
                        addConfirm();
                    }
                };
                imageCompressionAsyncTask.execute(selectedUri.getPath());
            }
        }
        catch(OutOfMemoryError o){
            o.printStackTrace();
            Alert.show(this, "Sorry, Out Of Memory Error", o.getMessage(), null);
        }
        catch(NullPointerException o){
            o.printStackTrace();
            Alert.show(this, "Sorry, Null Pointer Exception", "Unable to complete previous operation\ndue to low memory", null);
        } catch (Exception o) {
            o.printStackTrace();
            Alert.show(this, "Sorry, Error occured", "Please reload page", null);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("taskList", taskList);
        outState.putParcelable("tempUri", tempUri);
        outState.putParcelable("selectedUri", selectedUri);
    }

    public void onRestoreInstanceState(Bundle b){
        super.onRestoreInstanceState(b);
        taskList = (TaskList) b.getSerializable("taskList");
        tempUri = b.getParcelable("tempUri");
        selectedUri = b.getParcelable("selectedUri");
    }

    private void addConfirm() {
        View view = LayoutInflater.from(SelfieFragment.this).inflate(R.layout.confirm_view, ((LinearLayout)findViewById(R.id.root)), false);
        if (findViewById(R.id.confirm) != null) {
            ((LinearLayout)findViewById(R.id.root)).removeView(findViewById(R.id.confirm));
        }
        Button buttonOk = (Button) view.findViewById(R.id.button_ok);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String filename = selectedUri.getPath();
                Meta meta = new Meta(taskList.getIdPm(), "selfie", filename,
                        "image", Utils.getWatermarkFromImage(filename));
                meta.setIsUpload(Delivery.PENDING);
                meta.saveMerge();

                Intent intent = new Intent(SelfieFragment.this, PreBlockedAccessFragment.class);
                intent.putExtra("taskList", taskList);
                startActivity(intent);

                finish();
            }
        });
        ((LinearLayout)findViewById(R.id.root)).addView(view, 0, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    @Override
    public void onClick(View v) {//selfie
        long time = BaseActivity.getGpsTime();

        String file = Utils.getNewFilePath(time);
        File newFile = new File(file);

        Intent pickPhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        tempUri = Uri.fromFile(newFile);

        pickPhoto.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(file)));
        startActivityForResult(pickPhoto, Tag.REQUEST_CODE_CAMERA);
    }
}