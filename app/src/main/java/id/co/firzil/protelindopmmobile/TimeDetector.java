package id.co.firzil.protelindopmmobile;

import android.content.Context;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import java.util.Iterator;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by Rio Rizky Rainey on 26/06/2015.
 * rizkyrainey@gmail.com
 */
public class TimeDetector implements Runnable, LocationListener, GpsStatus.Listener {
    private static TimeDetector timeDetector;
    private static double latitude = 1000, longitude = 1000;

    private final ScheduledExecutorService executor;
    private Listener updateListeners;
    private Context activity;
    private long gpsTime;
    private LocationManager locationManager;
    private ScheduledFuture<?> updateTask;
    private long lastReadGps = 0;

    private TimeDetector() {
        this.executor = Executors.newSingleThreadScheduledExecutor();
    }

    public static TimeDetector getInstance() {
        if (timeDetector == null) {
            timeDetector = new TimeDetector();
        }
        return timeDetector;
    }

    public void setActivity(Context paramActivity) {
        this.activity = paramActivity;
        this.locationManager = ((LocationManager) paramActivity.getSystemService(Context.LOCATION_SERVICE));
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }


    public void addUpdateListener(Listener paramListener) {
        this.updateListeners = paramListener;
    }

    public void onGpsStatusChanged(int paramInt) {
        GpsStatus localGpsStatus = this.locationManager.getGpsStatus(null);
        int i = 0;
        Iterator localIterator = localGpsStatus.getSatellites().iterator();
        while (true) {
            if (!localIterator.hasNext()) {
                return;
            }
            if (((GpsSatellite) localIterator.next()).usedInFix())
                i++;
        }
    }

    public void onLocationChanged(Location paramLocation) {
        this.gpsTime = paramLocation.getTime();
        latitude = paramLocation.getLatitude();
        longitude = paramLocation.getLongitude();
        lastReadGps = this.gpsTime;
    }

    public void onProviderDisabled(String paramString) {

    }

    public void onProviderEnabled(String paramString) {
    }

    public void onStatusChanged(String paramString, int paramInt, Bundle paramBundle) {
    }

    protected void resetGpsInfo() {
        this.gpsTime = 0L;

        TimeDetector.latitude = 1000;
        TimeDetector.longitude = 1000;
        this.lastReadGps = 0;
    }

    public void run() {
        if (updateListeners != null) {
            gpsTime += 1000;
            updateListeners.onUpdate(System.currentTimeMillis(), gpsTime, lastReadGps, latitude, longitude);
        }
    }

    public void start() {
        if (updateTask == null) {
            Log.d(getClass().getName(), "start");
            if (this.activity == null)
                throw new Error("Activity must set before start");
            long timeMinimum = Long.parseLong(Me.getValue(activity, "min_time_update_gps"));
            long distanceMinimum = Long.parseLong(Me.getValue(activity, "min_distance_update_gps"));
            this.locationManager.requestLocationUpdates("gps", timeMinimum, distanceMinimum, this);
            this.locationManager.addGpsStatusListener(this);
            this.updateTask = this.executor.scheduleAtFixedRate(this, 0L, 1L, TimeUnit.SECONDS);
        } else Log.d(getClass().getName(), "already started, no need to start");
    }

    public void stop() {
        if (updateTask != null) {
            this.updateTask.cancel(false);
            this.locationManager.removeGpsStatusListener(this);
            this.locationManager.removeUpdates(this);
            resetGpsInfo();
            updateTask = null;
        }
    }

    public interface Listener {

        /**
         * @param paramLong1 current Time
         * @param paramLong2 gps time
         * @param paramInt   gps status
         */
        void onUpdate(long paramLong1, long paramLong2, long paramInt, double latitude, double longitude);
    }

}
