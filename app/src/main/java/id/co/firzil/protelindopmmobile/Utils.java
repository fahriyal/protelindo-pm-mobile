package id.co.firzil.protelindopmmobile;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import id.co.firzil.protelindopmmobile.fragment.BaseActivity;

/**
 * Created by Rio Rizky Rainey on 23/02/2015.
 * rizkyrainey@gmail.com
 */
public class Utils {

    public static String changeFormatDate(String date, String formatDateIn, String formatDateOut) {
        DateFormat dfIn = new SimpleDateFormat(formatDateIn);
        DateFormat dfOut = new SimpleDateFormat(formatDateOut);
        Date startDate;
        try {
            System.out.println("DATE: " + dfIn);
            startDate = dfIn.parse(date);
            return dfOut.format(startDate);
        } catch (ParseException e) {
            return date;
        } catch (NullPointerException e) {
            return date;
        }
    }

    public static String miliSecondToDateString(long millisecond) {
        TimeZone tz = TimeZone.getTimeZone("GMT+7");
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT);
        sdf.setTimeZone(tz);
        return sdf.format(millisecond);
//        return android.text.format.DateFormat.format(Constants.DATE_FORMAT, new Date(millisecond)).toString();
    }

    public static boolean isTextEmpty(String... text) {
        for (String aText : text) {
            if (TextUtils.isEmpty(aText))
                return true;
        }
        return false;
    }

    public static boolean isUnder16(){
        return Build.VERSION.SDK_INT < 16;
    }

    public static String getNewFilePath(long time){
        String eks = ".jpg";
        String s = "_";
        String nama = Constants.FILE_IMAGE + time;
        String fileName = nama + eks;
        File newFile = new File(fileName);
        try {
            int n;
            while(newFile.exists()) {
                String pecah[] = nama.split(s);
                String awal;
                if(pecah.length <= 1){
                    awal = nama;
                    n = 1;
                }
                else {
                    awal = pecah[pecah.length - 2];
                    n = Integer.parseInt(pecah[pecah.length - 1]) + 1;
                }

                nama = awal + s + n;
                fileName = nama + eks;
                newFile = new File(fileName);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileName;
    }

    public static String getWatermarkFromImage(String filename){
        String watermark = "";
        try {
            String waktu_foto = Utils.miliSecondToDateString(Long.parseLong(filename.replace(Constants.FILE_IMAGE, "").replace(".jpg", "").
                    replace("_", "")));

            if (! Utils.isTextEmpty(waktu_foto)) {
                watermark = waktu_foto + "#" + waktu_foto;
                String latitudeLongitude = "";

                ExifInterface exif = new ExifInterface(filename);
                String lat = exif.getAttribute(ExifInterface.TAG_GPS_LATITUDE);
                String longt = exif.getAttribute(ExifInterface.TAG_GPS_LONGITUDE);
                Float latDegree = GPSUtils.convertToDegree(lat, exif.getAttribute(ExifInterface.TAG_GPS_LATITUDE_REF));
                Float longDegree = GPSUtils.convertToDegree(longt, exif.getAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF));
                if (latDegree != null) latitudeLongitude = latDegree.toString();
                if (longDegree != null) latitudeLongitude += ("," + longDegree.toString());

                if (! TextUtils.isEmpty(latitudeLongitude)) watermark = watermark + "+" + latitudeLongitude;

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return watermark;
    }

    public static void makeToast(Context context, String text) {
        if (!Utils.isTextEmpty(text))
            Toast.makeText(context, text, Toast.LENGTH_LONG).show();
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();

        return netInfo != null && netInfo.isConnected();
    }

    public static String getImei(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }

    public static boolean isGPSEnabled(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static int isEnabledAutoTime(Context context) {
        if (Build.VERSION.SDK_INT <= 16)
            return android.provider.Settings.System.getInt(context.getContentResolver(), Settings.System.AUTO_TIME, 0);
        else
            return android.provider.Settings.Global.getInt(context.getContentResolver(), Settings.Global.AUTO_TIME, 0);
    }

    public static int isEnabledAutoTimeZone(Context context) {
        if (Build.VERSION.SDK_INT <= 16)
            return android.provider.Settings.System.getInt(context.getContentResolver(), Settings.System.AUTO_TIME_ZONE, 0);
        else
            return android.provider.Settings.Global.getInt(context.getContentResolver(), Settings.Global.AUTO_TIME_ZONE, 0);
    }

    public static boolean checkAutoTime(final Context context) {
        Intent intentSetting = null;
        int idTitleDialog = 0;
        int idMessageDialog = 0;

        if (Utils.isEnabledAutoTime(context) == 0 || Utils.isEnabledAutoTimeZone(context) == 0) {
            idTitleDialog = R.string.TimeAlertDialogTitle;
            idMessageDialog = R.string.TimeAlertDialogMessage;
            intentSetting = new Intent(Settings.ACTION_DATE_SETTINGS);
        }

        if (intentSetting != null) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
            alertDialog.setCancelable(false);

            // Setting Dialog Title
            alertDialog.setTitle(idTitleDialog);

            // Setting Dialog Message
            alertDialog.setMessage(idMessageDialog);

            // On Pressing Setting button
            final Intent finalIntentSetting = intentSetting;
            alertDialog.setPositiveButton(R.string.settings, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    context.startActivity(finalIntentSetting);
                }
            });

            // On pressing cancel button
            alertDialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    ((Activity) context).finish();
                }
            });
            alertDialog.show();
            return false;
        }
        return true;
    }

    /**
     * delete file or directory.
     * @param fileOrDirectory
     */
    public static void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        fileOrDirectory.delete();
    }

    public static String getAppVersion(Context context) {
        return context.getString(R.string.version);
    }

    public static int dpToPixel(Context context, int dp) {
        float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale);
    }
}
