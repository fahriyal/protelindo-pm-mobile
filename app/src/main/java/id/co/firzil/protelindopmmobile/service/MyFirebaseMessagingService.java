package id.co.firzil.protelindopmmobile.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.bugsnag.android.Bugsnag;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.Map;

import id.co.firzil.protelindopmmobile.Me;
import id.co.firzil.protelindopmmobile.R;
import id.co.firzil.protelindopmmobile.ThumbnailImage;
import id.co.firzil.protelindopmmobile.Utils;
import id.co.firzil.protelindopmmobile.fragment.PMListFragment;

/*
 * Created by Fahriyal Afif on 8/17/2016.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage){
        Context c = this;
        try{
            Map data = remoteMessage.getData();
            System.out.println("fcm message: "+data.toString());


            JSONObject pesan = new JSONObject(data.get("message").toString());
            JSONObject data_gcm = pesan.getJSONObject("data_gcm");
            if(Me.isLogin(c) && data_gcm.getString("pic_recipient").equalsIgnoreCase(Me.getValue(c, Me.IDVENDOR_USER))){
                String id_gcm = pesan.get("id_gcm").toString();
                String task = data_gcm.getString("task");
                String message = data_gcm.getString("message");

                JSONObject data_task = data_gcm.getJSONObject("data_task");
                int id_task = data_task.getInt("idpm_plan_detil");

                Class<?> cls = PMListFragment.class;
                Intent in = new Intent(c, cls);
                in.putExtra("pmstatus", data_task.getString("pmp_status"));
                in.putExtra("keyword", task);
                in.putExtra("periode", 0);

                String title;
                if(id_gcm.equalsIgnoreCase("report_rejected")) title = "Report Rejected";
                else if(id_gcm.equalsIgnoreCase("report_approved")) title = "Report Approved";
                else if(id_gcm.equalsIgnoreCase("new_task")) title = "New Task";
                else title = "Notification";

                createNotif(cls, in, id_task, title, message);
            }
        }
        catch (Exception e){
            e.printStackTrace();
            Bugsnag.init(c);
            Bugsnag.notify(e);
        }

    }

    public void createNotif(Class<?> cls, Intent in, int id_notifikasi, String title, String message){
        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(cls);
        stackBuilder.addNextIntent(in);

        PendingIntent p = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_ONE_SHOT |
                PendingIntent.FLAG_UPDATE_CURRENT | Intent.FILL_IN_DATA);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.pm)
                .setContentTitle(title)
                .setContentText(message)
                .setContentIntent(p)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setAutoCancel(true);
        int h = Utils.dpToPixel(this, 75);
        ThumbnailImage th = new ThumbnailImage();
        builder.setLargeIcon(th.loadBitmap(getResources(), R.drawable.pm, h / 2, h / 2));

        Notification notif = builder.build();
        notif.flags |= Notification.FLAG_SHOW_LIGHTS;
        notif.ledOnMS = 1000;
        notif.ledOffMS = 1000;
        notif.defaults = Notification.DEFAULT_LIGHTS;
        notif.defaults |= Notification.DEFAULT_SOUND;

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(id_notifikasi, notif);
    }

}
