package id.co.firzil.protelindopmmobile.ui;

import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

import id.co.firzil.protelindopmmobile.entity.TaskList;

/**
 * Created by Rio Rizky Rainey on 29/03/2015.
 * rizkyrainey@gmail.com
 */
public class SyncMetaNotification {

    private String idTask;
    private String idSite;

    private NotificationManager notificationManager;
    private NotificationCompat.Builder builder;

    public SyncMetaNotification(Context context, String idTask) {
        this.idTask = idTask;
        TaskList taskList = TaskList.findBy(TaskList.class, "idpm", idTask);
        idSite = taskList.getIdSite();
        notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        builder = new NotificationCompat.Builder(context);
        builder.setContentTitle("Sync Meta Data: " + idSite)
                .setContentText("Upload in progress")
                .setSmallIcon(android.R.drawable.ic_popup_sync);
    }

    public void setProgress(int max, int now) {
        builder.setContentTitle("Sync Meta Data: " + idSite)
                .setContentText("Upload in progress")
                .setSmallIcon(android.R.drawable.ic_popup_sync);
        builder.setProgress(max, now, false);
        notificationManager.notify(Integer.parseInt(idTask), builder.build());
    }

    public void uploadFailed() {
        builder.setContentTitle("Sync Meta Data: " + idSite)
                .setContentText("Offline,  looking for data signal").setProgress(0, 0, false)
                .setSmallIcon(android.R.drawable.ic_dialog_alert);
        notificationManager.notify(Integer.parseInt(idTask), builder.build());
    }

    public void loadReportSubmit() {
        builder.setContentTitle("Submit Report: " + idSite)
                .setContentText("Submitting").setProgress(0, 0, true)
                .setSmallIcon(android.R.drawable.ic_popup_sync);
        notificationManager.notify(Integer.parseInt(idTask), builder.build());
    }

    public void reportSubmitted() {
        builder.setContentTitle("Submit Report: " + idSite)
                .setContentText("Report has been submitted").setProgress(0, 0, false)
                .setSmallIcon(android.R.drawable.ic_dialog_info);
        notificationManager.notify(Integer.parseInt(idTask), builder.build());
    }

    public void reportFailed(String message) {
        builder.setContentTitle("Failed Submit Report: " + idSite)
                .setContentText(message).setProgress(0, 0, false)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setSmallIcon(android.R.drawable.ic_dialog_alert);
        notificationManager.notify(Integer.parseInt(idTask), builder.build());
    }

}
