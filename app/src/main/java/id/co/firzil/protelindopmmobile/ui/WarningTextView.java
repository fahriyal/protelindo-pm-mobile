package id.co.firzil.protelindopmmobile.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import id.co.firzil.protelindopmmobile.R;


/**
 * TODO: document your custom view class.
 */
public class WarningTextView extends TextView {

    private String text;
    private ViewGroup parent;

    private OnClickListener onClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if(parent!=null) {
                parent.removeView(view);
            } else {
                view.setVisibility(View.GONE);
            }
        }
    };

    public WarningTextView(Context context) {
        super(context);
        init(null, 0);
    }

    public WarningTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public WarningTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        setOnClickListener(onClickListener);
        setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.login_unsuccesfull));
        setTextColor(getContext().getResources().getColor(R.color.color_red_dark));
    }

    public void hide() {
        if (parent != null) {
            parent.removeView(this);
        } else {
            this.setVisibility(View.GONE);
        }
    }

    public void setParent(ViewGroup parent) {
        this.parent = parent;
    }

}
