package id.co.firzil.protelindopmmobile.entity;

import android.database.Cursor;
import android.text.TextUtils;

import com.gz.databaselibrary.GzDatabase;
import com.gz.databaselibrary.GzEntity;
import com.gz.databaselibrary.Utils;
import com.gz.databaselibrary.annotation.Column;
import com.gz.databaselibrary.annotation.Id;
import com.gz.databaselibrary.annotation.Table;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.gz.databaselibrary.GzApplication.getInstance;

/**
 * Created by Rio Rizky Rainey on 27/02/2015.
 * rizkyrainey@gmail.com
 */
@Table(name = "task_list")
public class TaskList extends GzEntity {

    public static final int BLOCKED_ACCESS=1, NOT_BLOCKED_ACCESS = 2;

    public static final int SUBMIT_NOT_YET = 0, SUBMIT_PENDING = 1,
            SUBMIT_SENDING = 2, SUBMIT_SUBMITTED = 3, SUBMIT_FAILED = 4;

    @Id
    @Column(name = "idpm")
    private Integer idPm;

    @Column(name = "pm_vendor")
    private String pmVendor;

    @Column(name = "Name")
    private String name;

    @Column(name = "period_name")
    private String pmPlan;

    @Column(name = "period_start_date")
    private String startDate;

    @Column(name = "period_end_date")
    private String endDate;

    @Column(name = "pmp_vendor_deadline_date")
    private String vendorDeadlineDate;

    @Column(name = "pm_description")
    private String description;

    @Column(name = "pmp_status")
    private String status;

    @Column(name = "pmp_idsite")
    private String idSite;

    @Column(name = "Region")
    private String region;

    @Column(name = "Province")
    private String province;

    @Column(name = "City")
    private String city;

    @Column(name = "Address")
    private String address;

    @Column(name = "Longitude")
    private String longitude;

    @Column(name = "Latitude")
    private String latitude;

    @Column(name = "Siteheight")
    private String siteHeight;

    @Column(name = "TowerType")
    private String powerType;

    @Column(name = "Anchor")
    private String anchor;

    @Column(name = "MechatronicPadlock")
    private String padlock;

    @Column(name = "idpm_plan_detil")
    private Integer idPmPlanDetil;

    @Column(name = "padlock")
    private String numberPadlock;

    @Column(name = "submitClicked")
    private Integer submitClicked;

    @Column(name = "hasBeenSubmitted")
    private Integer submissionStatus;

    @Column(name = "periode")
    private Integer periode;

    @Column(name = "mandatory")
    private String mandatory;

    @Column(name = "radius")
    private Double radius;

    @Column(name = "submit_date")
    private String submitDate;

    @Column(name = "OMSubcount")
    private String omSubcount;

    /**
     * 0 if pre blocked access never submitted
     * </br> 1 if blocked access
     * </br> -1 if not blocked access
     */
    @Column(name = "isBlockedAccess")
    private Integer isBlockedAccess = 0;

    public TaskList() {
    }

    public TaskList(Integer idPm, String pmVendor, String name, String pmPlan, String startDate,
                    String endDate, String vendorDeadlineDate, String description, String status,
                    String idSite, String region, String province, String city, String address,
                    String longitude, String latitude, String siteHeight, String powerType,
                    String anchor, String padlock, Integer idPmPlanDetil, String numberPadlock,
                    Integer submitClicked, Integer hasBeenSubmitted, Integer isBlockedAccess) {
        this.idPm = idPm;
        this.pmVendor = pmVendor;
        this.name = name;
        this.pmPlan = pmPlan;
        this.startDate = startDate;
        this.endDate = endDate;
        this.vendorDeadlineDate = vendorDeadlineDate;
        this.description = description;
        this.status = status;
        this.idSite = idSite;
        this.region = region;
        this.province = province;
        this.city = city;
        this.address = address;
        this.longitude = longitude;
        this.latitude = latitude;
        this.siteHeight = siteHeight;
        this.powerType = powerType;
        this.anchor = anchor;
        this.padlock = padlock;
        this.idPmPlanDetil = idPmPlanDetil;
        this.numberPadlock = numberPadlock;
        this.submitClicked = submitClicked;
        this.submissionStatus = hasBeenSubmitted;
        this.isBlockedAccess = isBlockedAccess;
        if (isBlockedAccess == null) {
            this.isBlockedAccess = 0;
        }
    }

    public static <T> List<T> submitPendingList() {
        String tableName = TaskList.class.getAnnotation(Table.class).name();
        String sql = "SELECT * FROM " + tableName + " WHERE hasBeenSubmitted=?";
        Cursor cursor = getInstance().getOpenHelper().getDatabase().rawQuery(sql, new String[]{""+SUBMIT_PENDING});
        List<T> list = new ArrayList<T>();
        try {
            while (cursor.moveToNext()) {
                @SuppressWarnings("unchecked")
                T entity = (T) Utils.setEntityFromCursor(TaskList.class, cursor);
                list.add(entity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }
        return list;
    }

    public static void kosongkan (Class<?> classType, String pmpStatus, int pmPeriode, String keyword, String... idpms) {
        String tableName = classType.getAnnotation(Table.class).name();

        String syarat_lagi = "";

        if(idpms != null && idpms.length > 0) {
            String where = "(";
            for (String anIdpm : idpms) {
                where += ("," + anIdpm);
            }
            where = where.replaceFirst(",", "") + ")";
            syarat_lagi = "AND idpm NOT IN "+where;
        }
        getInstance().getOpenHelper().getDatabase().delete(tableName,
                "pmp_status=? AND periode=? AND (Name LIKE  '%" + keyword + "%' OR pmp_idsite LIKE '%" + keyword + "%') "+syarat_lagi,
                new String[]{pmpStatus, pmPeriode + ""});
    }

    public static <T> List<T> findByPmpStatus(Class<?> classType, String pmpStatus, int pmPeriode, String keyword) {
        String tableName = classType.getAnnotation(Table.class).name();
        String sql = "SELECT * FROM " + tableName + " WHERE pmp_status=? AND periode=? AND (Name LIKE  '%" + keyword + "%' OR pmp_idsite LIKE '%" + keyword + "%')";
        Cursor cursor = getInstance().getOpenHelper().getDatabase().rawQuery(sql, new String[]{pmpStatus, pmPeriode + ""});
        List<T> list = new ArrayList<T>();
        try {
            while (cursor.moveToNext()) {
                @SuppressWarnings("unchecked")
                T entity = (T) Utils.setEntityFromCursor(classType, cursor);
                list.add(entity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }
        return list;
    }

    public static boolean isHasData(String pmpStatus) {
        String tableName = Utils.getTableAnnotation(TaskList.class).name();
        String sql = "SELECT COUNT(*) FROM " + tableName + " WHERE pmp_status=?";
        Cursor cursor = GzDatabase.getInstance().getDatabase().rawQuery(sql, new String[]{pmpStatus});
        int count = 0;
        while (cursor.moveToNext()) {
            count = cursor.getInt(0);
        }
        cursor.close();
        return count > 0;
    }

    public Integer getIdPm() {
        return idPm;
    }

    public void setIdPm(Integer idPm) {
        this.idPm = idPm;
    }

    public String getPmVendor() {
        return pmVendor;
    }

    public void setPmVendor(String pmVendor) {
        this.pmVendor = pmVendor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPmPlan() {
        return pmPlan;
    }

    public void setPmPlan(String pmPlan) {
        this.pmPlan = pmPlan;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIdSite() {
        return idSite;
    }

    public void setIdSite(String idSite) {
        this.idSite = idSite;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getSiteHeight() {
        return siteHeight;
    }

    public void setSiteHeight(String siteHeight) {
        this.siteHeight = siteHeight;
    }

    public String getPowerType() {
        return powerType;
    }

    public void setPowerType(String powerType) {
        this.powerType = powerType;
    }

    public String getAnchor() {
        return anchor;
    }

    public void setAnchor(String anchor) {
        this.anchor = anchor;
    }

    public String getPadlock() {
        return padlock;
    }

    public void setPadlock(String padlock) {
        this.padlock = padlock;
    }

    public Integer getIdPmPlanDetil() {
        return idPmPlanDetil;
    }

    public void setIdPmPlanDetil(Integer idPmPlanDetil) {
        this.idPmPlanDetil = idPmPlanDetil;
    }

    public String getNumberPadlock() {
        return numberPadlock;
    }

    public void setNumberPadlock(String numberPadlock) {
        this.numberPadlock = numberPadlock;
    }

    public String getVendorDeadlineDate() {
        return vendorDeadlineDate;
    }

    public void setVendorDeadlineDate(String vendorDeadlineDate) {
        this.vendorDeadlineDate = vendorDeadlineDate;
    }

    public Integer getSubmissionStatus() {
        return submissionStatus;
    }

    /**
     * 0 belum disubmit.
     * 1 sudah disubmit.
     * 2 masih proses.
     *
     * @param submissionStatus
     */
    public void setSubmissionStatus(Integer submissionStatus) {
        this.submissionStatus = submissionStatus;
        super.saveMerge();
    }

    public Integer getPeriode() {
        return periode;
    }

    public void setPeriode(Integer periode) {
        this.periode = periode;
    }

    public String getMandatory() {
        return mandatory;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public String getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(String submitDate) {
        this.submitDate = submitDate;
    }

    public String getOmSubcount() {
        return omSubcount;
    }

    public void setOmSubcount(String omSubcount) {
        this.omSubcount = omSubcount;
    }

    /**
     * 0 if pre blocked access never submitted
     * </br> 1 if blocked access
     * </br> -1 if not blocked access
     */
    public Integer getIsBlockedAccess() {
        return isBlockedAccess;
    }

    public void setIsBlockedAccess(Integer isBlockedAccess) {
        this.isBlockedAccess = isBlockedAccess;
        System.out.println("sip: "+this.isBlockedAccess);
    }

    public Double getRadius() {
        return radius;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    @Override
    public void saveMerge() {
        TaskList taskList = findById(getClass(), getId());
        if(taskList!= null) {
            /*isBlockedAccess = taskList.getIsBlockedAccess();
            submissionStatus = taskList.getSubmissionStatus();*/

            if (getPeriode() == 0 && taskList.getPeriode() != 0)
                periode = taskList.getPeriode();
            if (getRadius() == 0 && taskList.getRadius() != 0)
                radius = taskList.getRadius();
            if (!TextUtils.isEmpty(taskList.getMandatory())) {
                if (TextUtils.isEmpty(mandatory)) {
                    mandatory = taskList.getMandatory();
                }
            }
            if (!TextUtils.isEmpty(taskList.getSubmitDate())) {
                if (TextUtils.isEmpty(submitDate)) {
                    submitDate = taskList.getSubmitDate();
                }
            }
        }
        if (isBlockedAccess == null) {
            isBlockedAccess = 0;
        }
        if (periode == null) {
            periode = 0;
        }
        if (radius == null) {
            radius = 0.0;
        }
        System.out.println("savemerge: "+isBlockedAccess);
        super.saveMerge();
    }

    @Override
    public void initFromJSONObject(JSONObject jsonObject) {
        super.initFromJSONObject(jsonObject);

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskList taskList = (TaskList) o;
        return idPm.equals(taskList.idPm);
    }

    @Override
    public int hashCode() {
        return idPm.hashCode();
    }

    @Override
    public String toString() {
        return "TaskList{" +
                "idPm=" + idPm +
                ", pmVendor='" + pmVendor + '\'' +
                ", name='" + name + '\'' +
                ", pmPlan='" + pmPlan + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", vendorDeadlineDate='" + vendorDeadlineDate + '\'' +
                ", description='" + description + '\'' +
                ", status='" + status + '\'' +
                ", idSite='" + idSite + '\'' +
                ", region='" + region + '\'' +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", address='" + address + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", siteHeight='" + siteHeight + '\'' +
                ", powerType='" + powerType + '\'' +
                ", anchor='" + anchor + '\'' +
                ", padlock='" + padlock + '\'' +
                ", idPmPlanDetil=" + idPmPlanDetil +
                ", numberPadlock='" + numberPadlock + '\'' +
                ", submitClicked=" + submitClicked +
                ", submissionStatus=" + submissionStatus +
                ", periode=" + periode +
                ", mandatory='" + mandatory + '\'' +
                ", radius=" + radius +
                ", isBlockedAccess=" + isBlockedAccess +
                '}';
    }
}
