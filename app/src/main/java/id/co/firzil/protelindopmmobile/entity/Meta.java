package id.co.firzil.protelindopmmobile.entity;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.gz.databaselibrary.GzDatabase;
import com.gz.databaselibrary.GzEntity;
import com.gz.databaselibrary.Utils;
import com.gz.databaselibrary.annotation.Column;
import com.gz.databaselibrary.annotation.Id;
import com.gz.databaselibrary.annotation.Table;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.gz.databaselibrary.GzApplication.getInstance;

/**
 * Created by Rio Rizky Rainey on 03/03/2015.
 * rizkyrainey@gmail.com
 */
@Table(name = "meta")
public class Meta extends GzEntity {

    @Id
    @Column(name = "idmeta")
    private Integer idMeta;

    @Column(name = "meta_table_reff_id")
    private Integer metaTableReffId;

    @Column(name = "meta_name")
    private String metaName;

    @Column(name = "meta_value")
    private String metaValue;

    @Column(name = "meta_input_type")
    private String metaInputType;

    @Column(name = "meta_date")
    private String metaDate;

    @Column(name = "is_upload")
    private Integer isUpload;

    public Meta() {
    }

    public Meta(Integer metaTableReffId, String metaName, String metaValue, String metaInputType, String metaDate) {
        if(metaValue == null) metaValue = "";

        this.metaTableReffId = metaTableReffId;
        this.metaName = metaName;
        this.metaValue = metaValue;
        this.metaInputType = metaInputType;
        this.metaDate = metaDate;
    }

    public static <T> List<T> findByIdPM(String idPm) {
        String tableName = Meta.class.getAnnotation(Table.class).name();
        String sql = "SELECT * FROM " + tableName + " WHERE meta_table_reff_id=?";
        Cursor cursor = getInstance().getOpenHelper().getDatabase().rawQuery(sql, new String[]{idPm});
        List<T> list = new ArrayList<T>();
        try {
            while (cursor.moveToNext()) {
                @SuppressWarnings("unchecked")
                T entity = (T) Utils.setEntityFromCursor(Meta.class, cursor);
                list.add(entity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }
        return list;
    }

    public static boolean isSelfieExist(String idPm) {
        String tableName = Utils.getTableAnnotation(Meta.class).name();
        String sql = "SELECT COUNT(*) FROM " + tableName + " WHERE meta_table_reff_id=? AND meta_name=?";
        Cursor cursor = getInstance().getOpenHelper().getDatabase().rawQuery(sql, new String[]{idPm, "selfie"});
        int count = 0;
        while (cursor.moveToNext()) {
            count = cursor.getInt(0);
        }
        cursor.close();
        return count > 0;
    }

    public static Meta getMeta(String idPm, String metaName, String metaType) {
        String tableName = Utils.getTableAnnotation(Meta.class).name();
        String sql = "SELECT * FROM " + tableName + " WHERE meta_table_reff_id=? AND meta_name=? AND meta_input_type=?";
        Cursor cursor = getInstance().getOpenHelper().getDatabase().rawQuery(sql, new String[]{idPm, metaName, metaType});
        List<Meta> list = new ArrayList<>();
        try {
            while (cursor.moveToNext()) {
                @SuppressWarnings("unchecked")
                Meta entity = Utils.setEntityFromCursor(Meta.class, cursor);
                list.add(entity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }
        Collections.sort(list, new Comparator<Meta>() {
            public int compare(Meta meta1, Meta meta2) {
//                DateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT, Locale.ENGLISH);
//                Date dateMeta1 = new Date();
//                Date dateMeta2 = new Date();
//                try {
//                    dateMeta1 = format.parse(meta1.getMetaDate());
//                    dateMeta2 = format.parse(meta2.getMetaDate());
//                } catch (ParseException e) {
//                }
//                return dateMeta1.compareTo(dateMeta2);
                return meta1.getIdMeta().compareTo(meta2.getIdMeta());
            }
        });
        if (list.size() > 0)
            return list.get(list.size() - 1);
        else
            return null;
    }

    public static Meta findByPathImage(String path) {
        String tableName = Utils.getTableAnnotation(Meta.class).name();
        String sql = "SELECT * FROM " + tableName + " WHERE meta_value=?";
        Cursor cursor = getInstance().getOpenHelper().getDatabase().rawQuery(sql, new String[]{path});
        Meta entity = null;
        try {
            while (cursor.moveToNext()) {
                entity = (Meta) Utils.setEntityFromCursor(Meta.class, cursor);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }
        return entity;
    }

    public static boolean isHasData(String idPm) {
        String tableName = Utils.getTableAnnotation(Meta.class).name();
        String sql = "SELECT COUNT(*) FROM " + tableName + " WHERE meta_table_reff_id=?";
        Cursor cursor = GzDatabase.getInstance().getDatabase().rawQuery(sql, new String[]{idPm});
        int count = 0;
        while (cursor.moveToNext()) {
            count = cursor.getInt(0);
        }
        cursor.close();
        return count > 0;
    }

    public static List<Meta> findBlockedAcces(String idPm) {
        String tableName = Meta.class.getAnnotation(Table.class).name();
        String sql = "SELECT * FROM " + tableName + " WHERE meta_table_reff_id=? AND meta_name like '%blocked#%'";
        Cursor cursor = getInstance().getOpenHelper().getDatabase().rawQuery(sql, new String[]{idPm});
        List<Meta> list = new ArrayList<Meta>();
        try {
            while (cursor.moveToNext()) {
                @SuppressWarnings("unchecked")
                Meta entity = Utils.setEntityFromCursor(Meta.class, cursor);
                list.add(entity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }
        return list;
    }

    public Integer getIdMeta() {
        return idMeta;
    }

    public void setIdMeta(Integer idMeta) {
        this.idMeta = idMeta;
    }

    public Integer getMetaTableReffId() {
        return metaTableReffId;
    }

    public void setMetaTableReffId(Integer metaTableReffId) {
        this.metaTableReffId = metaTableReffId;
    }

    public String getMetaName() {
        return metaName;
    }

    public void setMetaName(String metaName) {
        this.metaName = metaName;
    }

    public String getMetaValue() {
        return metaValue;
    }

    public void setMetaValue(String metaValue) {
        this.metaValue = metaValue;
    }

    public String getMetaInputType() {
        return metaInputType;
    }

    public void setMetaInputType(String metaInputType) {
        this.metaInputType = metaInputType;
    }

    public String getMetaDate() {
        return metaDate;
    }

    public void setMetaDate(String metaDate) {
        this.metaDate = metaDate;
    }

    public Integer getIsUpload() {
        return isUpload;
    }

    public void setIsUpload(Integer isUpload) {
        this.isUpload = isUpload;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Meta meta = (Meta) o;

        if (!metaInputType.equals(meta.metaInputType)) return false;
        if (!metaName.equals(meta.metaName)) return false;
        if (!metaTableReffId.equals(meta.metaTableReffId)) return false;
        if (!metaValue.equals(meta.metaValue)) return false;

        return true;
    }

    @Override
    public String toString() {
        return "Meta{" +
                "idMeta=" + idMeta +
                ", metaTableReffId=" + metaTableReffId +
                ", metaName='" + metaName + '\'' +
                ", metaValue='" + metaValue + '\'' +
                ", metaInputType='" + metaInputType + '\'' +
                ", metaDate='" + metaDate + '\'' +
                ", isUpload='" + isUpload + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        int result = metaTableReffId.hashCode();
        result = 31 * result + metaName.hashCode();
        result = 31 * result + metaValue.hashCode();
        result = 31 * result + metaInputType.hashCode();
        return result;
    }

    @Override
    public void saveMerge() {
        String tableName = Utils.getTableAnnotation(getClass()).name();
        String sql = "SELECT * FROM " + tableName + " WHERE meta_table_reff_id=? AND meta_name=? AND meta_input_type=?";
        Cursor cursor = GzDatabase.getInstance().getDatabase().rawQuery(sql, new String[]{metaTableReffId + "", metaName, metaInputType});
        Integer idMeta = 0;
        String lastValue = "";
        while (cursor.moveToNext()) {
            idMeta = cursor.getInt(cursor.getColumnIndex("idmeta"));
            lastValue = cursor.getString(cursor.getColumnIndex("meta_value"));
        }
        cursor.close();

        Log.d("Ya Allah "+metaName, "Ya Allah "+lastValue +" --- "+metaValue);

        if (idMeta != 0) {
            this.idMeta = idMeta;
            if( ! lastValue.equals(metaValue)) {
                update();
                Log.d("Ya Allah "+idMeta, "Ya Allah UPDATE");
            }
            else Log.d("Ya Allah "+idMeta, "Ya Allah VALUE SAMA");
        } else {
            save();
            Log.d("Ya Allah " + idMeta, "Ya Allah SAVE");
        }
    }

    public void updateStatusUpload(int status){
        ContentValues v = new ContentValues();
        v.put("is_upload", status);
        GzDatabase.getInstance().getDatabase().update(Utils.getTableAnnotation(getClass()).name(), v,
                "meta_table_reff_id=? AND meta_name=?", new String[]{metaTableReffId + "", metaName});
    }

    public void updateMetaDate(String date){
        ContentValues v = new ContentValues();
        v.put("meta_date", date);
        GzDatabase.getInstance().getDatabase().update(Utils.getTableAnnotation(getClass()).name(), v,
                "meta_table_reff_id=? AND meta_name=?", new String[]{metaTableReffId + "", metaName});
    }

}
