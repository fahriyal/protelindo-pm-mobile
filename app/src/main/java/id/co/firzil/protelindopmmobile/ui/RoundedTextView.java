package id.co.firzil.protelindopmmobile.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.widget.TextView;

import id.co.firzil.protelindopmmobile.R;

public class RoundedTextView extends TextView {

    protected Drawable gradientDrawable;
    private int colorBackground = Color.TRANSPARENT;

    public RoundedTextView(Context context) {
        super(context);
        init(null, 0);
    }

    public RoundedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public RoundedTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        setBackgroundResource(R.drawable.rounded_yellow);
        GradientDrawable drawable = (GradientDrawable) getBackground();
        drawable.setColor(colorBackground);
    }

    private void init(AttributeSet attrs, int defStyle) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.RoundedTextView);
            colorBackground = a.getColor(0, Color.TRANSPARENT);
            a.recycle();
        }
    }

    public void setColorBackground(int color) {
        this.colorBackground = color;
        postInvalidate();
    }

}
