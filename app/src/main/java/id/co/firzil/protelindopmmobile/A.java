package id.co.firzil.protelindopmmobile;

import id.co.firzil.protelindopmmobile.ui.UploadButton;

/**
 * Created by fahriyalafif on 3/18/2016.
 */
public class A {
    private static A main;
    private UploadButton b;
    public static boolean start_camera = false;

    public static A get(){
        if(main == null) main = new A();
        return main;
    }

    private A(){}

    public UploadButton getButton(){
        return b;
    }

    public void setButton(UploadButton b){
        this.b = b;
    }

}
