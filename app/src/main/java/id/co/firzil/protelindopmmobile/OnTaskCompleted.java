package id.co.firzil.protelindopmmobile;

/**
 * Created by Rio Rizky Rainey on 24/02/2015.
 * rizkyrainey@gmail.com
 */
public interface OnTaskCompleted {
    void onTaskCompleted(Object object);
}
