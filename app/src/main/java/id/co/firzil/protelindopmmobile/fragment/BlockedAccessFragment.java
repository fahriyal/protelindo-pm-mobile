package id.co.firzil.protelindopmmobile.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;

import id.co.firzil.protelindopmmobile.A;
import id.co.firzil.protelindopmmobile.BaseAsyncTask;
import id.co.firzil.protelindopmmobile.Delivery;
import id.co.firzil.protelindopmmobile.ImageCompressionAsyncTask;
import id.co.firzil.protelindopmmobile.PreparingUpload;
import id.co.firzil.protelindopmmobile.R;
import id.co.firzil.protelindopmmobile.Tag;
import id.co.firzil.protelindopmmobile.Utils;
import id.co.firzil.protelindopmmobile.entity.Meta;
import id.co.firzil.protelindopmmobile.entity.TaskList;
import id.co.firzil.protelindopmmobile.service.SubmitAnyarService;
import id.co.firzil.protelindopmmobile.ui.Alert;
import id.co.firzil.protelindopmmobile.ui.UploadButton;

/**
 * Created by Rio Rizky Rainey on 30/03/2015.
 * rizkyrainey@gmail.com
 */
public class BlockedAccessFragment extends BaseActivity {

    private EditText bresingEditText;
    private EditText pondasiEditText;
    private EditText currentStatusEditText;
    private UploadButton uploadButton;
    private UploadButton uploadButton2;
    private UploadButton uploadButton3;
    private UploadButton uploadButton4;

    private Integer idPm;
    private ProgressDialog dialogPreparingUpload;

    private BroadcastReceiver uploadReadyReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent in) {
            if(in.getAction().equalsIgnoreCase(PMListFragment.INTENT_FILTER_UPLOAD_READY)){
                try{
                    dialogPreparingUpload.dismiss();
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                finish();
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(1, R.id.restart_gps, 1, getString(R.string.action_restart_gps)).setEnabled(true);

        return true;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        idPm = getIntent().getExtras().getInt("idPm");
        setContentView(R.layout.fragment_blocked_access);
        setNavbar();
        setTitle("PM Form");
        bresingEditText = (EditText) findViewById(R.id.edit_bresing);
        pondasiEditText = (EditText) findViewById(R.id.edit_pondasiTower);
        currentStatusEditText = (EditText) findViewById(R.id.edit_currentStatus);
        uploadButton = (UploadButton) findViewById(R.id.button_photo);
        uploadButton2 = (UploadButton) findViewById(R.id.button_photo2);
        uploadButton3 = (UploadButton) findViewById(R.id.button_photo3);
        uploadButton4 = (UploadButton) findViewById(R.id.button_photo4);
        Button submitButton = (Button) findViewById(R.id.button_submit);


        load();
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String bresing = bresingEditText.getText().toString();
                String pondasi = pondasiEditText.getText().toString();
                String currentStatus = currentStatusEditText.getText().toString();
                String pathPhoto = uploadButton.getImagePath();
                String pathPhoto2 = uploadButton2.getImagePath();
                String pathPhoto3 = uploadButton3.getImagePath();
                String pathPhoto4 = uploadButton4.getImagePath();
                if (Utils.isTextEmpty(pathPhoto, pathPhoto2, pathPhoto3, pathPhoto4)) {
                    Utils.makeToast(BlockedAccessFragment.this, "Anda belum mengambil foto");
                }
                else if (Utils.isTextEmpty( bresing, pondasi, currentStatus)) {
                    Utils.makeToast(BlockedAccessFragment.this, "Masih ada field yang kosong");
                }
                else {
                    TaskList taskList = TaskList.findById(TaskList.class, "" + idPm);


                    taskList.setSubmissionStatus(TaskList.SUBMIT_PENDING);
                    taskList.saveMerge();

                    if (dialogPreparingUpload == null) dialogPreparingUpload = new ProgressDialog(c);
                    new PreparingUpload(c, taskList, dialogPreparingUpload).execute();

                    String date = Utils.miliSecondToDateString(getGpsTime());
                    Meta bresingMeta = new Meta(idPm, "blocked#bresing", bresing, "text", date);
                    Meta pondasiMeta = new Meta(idPm, "blocked#pondasi_tower", pondasi, "text", date);
                    Meta currentStatusMeta = new Meta(idPm, "blocked#current_status", currentStatus, "text", date);

                    Meta photoMeta = new Meta(idPm, "blocked#photo", pathPhoto, "image", Utils.getWatermarkFromImage(pathPhoto));
                    Meta photoMeta2 = new Meta(idPm, "blocked#photo2", pathPhoto2, "image", Utils.getWatermarkFromImage(pathPhoto2));
                    Meta photoMeta3 = new Meta(idPm, "blocked#photo3", pathPhoto3, "image", Utils.getWatermarkFromImage(pathPhoto3));
                    Meta photoMeta4 = new Meta(idPm, "blocked#photo4", pathPhoto4, "image", Utils.getWatermarkFromImage(pathPhoto4));

                    bresingMeta.setIsUpload(Delivery.PENDING);
                    pondasiMeta.setIsUpload(Delivery.PENDING);
                    currentStatusMeta.setIsUpload(Delivery.PENDING);
                    photoMeta.setIsUpload(Delivery.PENDING);
                    photoMeta2.setIsUpload(Delivery.PENDING);
                    photoMeta3.setIsUpload(Delivery.PENDING);
                    photoMeta4.setIsUpload(Delivery.PENDING);

                    bresingMeta.saveMerge();
                    pondasiMeta.saveMerge();
                    currentStatusMeta.saveMerge();
                    photoMeta.saveMerge();
                    photoMeta2.saveMerge();
                    photoMeta3.saveMerge();
                    photoMeta4.saveMerge();

                    startService(new Intent(c, SubmitAnyarService.class));
                }
            }
        });

        LocalBroadcastManager.getInstance(c).registerReceiver(uploadReadyReceiver, new IntentFilter(PMListFragment.INTENT_FILTER_UPLOAD_READY));
    }

    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(c).unregisterReceiver(uploadReadyReceiver);
    }

    private void load() {
        BaseAsyncTask baseAsyncTask = new BaseAsyncTask(BlockedAccessFragment.this, "Tunggu...") {
            @Override
            protected Object doInBackground(Object[] params) {
                List<Meta> metaList = Meta.findBlockedAcces("" + idPm);
                for (final Meta meta : metaList) {
                    if (meta.getMetaName().equalsIgnoreCase("blocked#bresing")) {
                        BlockedAccessFragment.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                bresingEditText.setText(meta.getMetaValue());
                            }
                        });
                    } else if (meta.getMetaName().equalsIgnoreCase("blocked#pondasi_tower")) {
                        BlockedAccessFragment.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pondasiEditText.setText(meta.getMetaValue());
                            }
                        });
                    } else if (meta.getMetaName().equalsIgnoreCase("blocked#current_status")) {
                        BlockedAccessFragment.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                currentStatusEditText.setText(meta.getMetaValue());
                            }
                        });
                    } else if (meta.getMetaName().equalsIgnoreCase("blocked#photo")) {
                        BlockedAccessFragment.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                uploadButton.setImageTrulyPath(meta.getMetaValue());
                            }
                        });
                    } else if (meta.getMetaName().equalsIgnoreCase("blocked#photo2")) {
                        BlockedAccessFragment.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                uploadButton2.setImageTrulyPath(meta.getMetaValue());
                            }
                        });
                    } else if (meta.getMetaName().equalsIgnoreCase("blocked#photo3")) {
                        BlockedAccessFragment.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                uploadButton3.setImageTrulyPath(meta.getMetaValue());
                            }
                        });
                    } else if (meta.getMetaName().equalsIgnoreCase("blocked#photo4")) {
                        BlockedAccessFragment.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                uploadButton4.setImageTrulyPath(meta.getMetaValue());
                            }
                        });
                    }
                }
                return null;
            }
        };
        baseAsyncTask.execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == Activity.RESULT_OK && requestCode == Tag.REQUEST_CODE_CAMERA) {
                String fileLocation = A.get().getButton().getTempPath();
                A.get().getButton().setImageTrulyPath(fileLocation);
                new ImageCompressionAsyncTask(this, null).execute(fileLocation);
            }
        } catch(OutOfMemoryError o){
            o.printStackTrace();
            Alert.show(this, "Sorry, Out Of Memory Error", o.getMessage(), null);
        }
        catch(NullPointerException o){
            o.printStackTrace();
            Alert.show(this, "Sorry, Null Pointer Exception", "Unable to complete previous operation\ndue to low memory", null);
        } catch (Exception o) {
            o.printStackTrace();
            Alert.show(this, "Sorry, Error occured", "Please reload page", null);
        }
    }

}