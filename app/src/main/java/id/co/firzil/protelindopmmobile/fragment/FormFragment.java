package id.co.firzil.protelindopmmobile.fragment;

import android.app.AlertDialog;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import id.co.firzil.protelindopmmobile.A;
import id.co.firzil.protelindopmmobile.BaseAsyncTask;
import id.co.firzil.protelindopmmobile.Constants;
import id.co.firzil.protelindopmmobile.Delivery;
import id.co.firzil.protelindopmmobile.R;
import id.co.firzil.protelindopmmobile.Utils;
import id.co.firzil.protelindopmmobile.entity.Meta;
import id.co.firzil.protelindopmmobile.entity.TaskList;
import id.co.firzil.protelindopmmobile.entity.Tenant;
import id.co.firzil.protelindopmmobile.ui.AcView;
import id.co.firzil.protelindopmmobile.ui.UploadButton;

/**
 * Created by Rio Rizky Rainey on 03/03/2015.
 * rizkyrainey@gmail.com
 */
public class FormFragment extends BaseFragment {

    public View view;
    private int idLayoutTerakhir;
    private Integer idPmTerakhir;
    private Tenant tenantTerakhir;
    private int idLayout;
    private Integer idPm;
    private Integer tenantCount;
    private String date;
    private Tenant tenant;
    private LinearLayout viewGroups;
    private String groupText;
    private LinearLayout indoorView;
    private EditText acEditText;
    private TaskList taskList;
    private BaseAsyncTask baseAsyncTaskLoad;
    private List<AcView> acViewList;
    private boolean onResume;
    private View.OnClickListener done_onclick;

    public void setDoneOnClickListener(View.OnClickListener done_onclick){
        this.done_onclick = done_onclick;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setGroupText(String groupText) {
        this.groupText = groupText;
    }

    public void setTenantCount(Integer tenantCount) {
        this.tenantCount = tenantCount;
    }

    public void setTaskList(TaskList taskList) {
        this.taskList = taskList;
    }

    public void setIdLayout(int page) {
        if (page == 0) {
            idLayout = R.layout.page_1;
        } else if (page == 1) {
            idLayout = R.layout.page_2;
        } else if (page == 2) {
            idLayout = R.layout.page_3;
        } else if (page == 3) {
            idLayout = R.layout.page_4;
        } else if (page == 4) {
            idLayout = R.layout.page_5;
        } else if (page == tenantCount + 5) {
            idLayout = R.layout.page_8;
        } else if (page == tenantCount + 5 + 1) {
            idLayout = R.layout.page_9;
        } else if (page == tenantCount + 5 + 2) {
            idLayout = R.layout.page_10;
        } else {
            idLayout = R.layout.page_6;
        }
    }

    public void setIdPm(Integer idPm) {
        this.idPm = idPm;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("taskList", taskList);
        outState.putSerializable("tenant", tenant);
        outState.putInt("idPm", idPm);
        outState.putInt("idLayout", idLayout);
        outState.putInt("tenantCount", tenantCount);
        outState.putString("date", date);
        outState.putString("groupText", groupText);
    }

    @Override
    public void createView() {
        onResume = false;

        if(savedInstanceState != null){
            System.out.println("JOOOOOOSSSSS");
            idPm = savedInstanceState.getInt("idPm");
            idLayout = savedInstanceState.getInt("idLayout");
            tenantCount = savedInstanceState.getInt("tenantCount");
            taskList = (TaskList) savedInstanceState.getSerializable("taskList");
            tenant = (Tenant) savedInstanceState.getSerializable("tenant");
            date = savedInstanceState.getString("date");
            groupText = savedInstanceState.getString("groupText");
        }

        if (idLayout != 0) {
            idLayoutTerakhir = idLayout;
            idPmTerakhir = idPm;
            tenantTerakhir = tenant;
        }
        if (tenantTerakhir == null)
            setContentView(idLayoutTerakhir);
        else{
            System.out.println("equipment type: " + tenantTerakhir.getEquipmentType() + " - maintenance: " + tenantTerakhir.getMaintenance());
            if (tenantTerakhir.getMaintenance() == 0) {
                setContentView(R.layout.form_no_maintenance);
                System.out.println("not maintained");
            }
            else if (tenantTerakhir.getEquipmentType() < 1 || tenantTerakhir.getEquipmentType() > 2) {
                setContentView(R.layout.form_equipment_null);
                System.out.println("equipment null");
            }
            else setContentView(idLayoutTerakhir);
        }
        viewGroups = (LinearLayout) findViewById(R.id.linear);
        if (findViewById(R.id.textView_tenantName) != null) {
            ((TextView) findViewById(R.id.textView_tenantName)).setText(Html.fromHtml("<u><b>TS-" + groupText + "</b></u>"));
        }
        LinearLayout layout = (LinearLayout) findViewById(R.id.indoor);
        if (layout != null) {
            String type;
            if (tenantTerakhir.getEquipmentType() == 1) {
                type = "indoor";
                System.out.println("INDOOR");
                acViewList = new ArrayList<>();
                indoorView = (LinearLayout) getInflater().inflate(R.layout.form_indoor, viewGroups, false);
                layout.addView(indoorView);

                acEditText = (EditText) findViewById(R.id.editText_ac);
                String metaNameAc = groupText.toLowerCase() + "#jumlah_ac";
                Meta meta = Meta.getMeta("" + idPmTerakhir, metaNameAc, "text");
                System.out.println("MetaName:" + metaNameAc + "  Ac: " + meta);
                if (meta != null) {
                    acEditText.setText(meta.getMetaValue());
                }
                Button acButton = (Button) findViewById(R.id.button_ac);
                acButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!Utils.isTextEmpty(acEditText.getText().toString())) {
                            int indexLayout = indoorView.indexOfChild(findViewById(R.id.ac));
                            int howManyAc = Integer.parseInt(acEditText.getText().toString());
                            if (howManyAc < 1 || howManyAc > 4) {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage("Jumlah ac mulai 1 hingga 4");
                                alertDialogBuilder.setTitle("Kesalahan input");
                                alertDialogBuilder.show();
                            } else if (acViewList.size() > howManyAc) {
                                for (int i = acViewList.size() - 1; i >= howManyAc; i--) {
                                    indoorView.removeView(acViewList.get(i));
                                    acViewList.remove(i);
                                }
                            } else if (acViewList.size() < howManyAc) {
                                for (int i = acViewList.size(); i < howManyAc; i++) {
                                    final AcView acView = new AcView(getActivity(), i + 1);
                                    indoorView.addView(acView, indexLayout + 1 + i);
                                    acViewList.add(acView);
                                    if (onResume) {
                                        baseAsyncTaskLoad = new BaseAsyncTask(getActivity(), "Load...") {
                                            @Override
                                            protected Object doInBackground(Object[] params) {
                                                load(acView, null);
                                                return null;
                                            }
                                        };
                                        baseAsyncTaskLoad.execute();
                                    }
                                }
                            }
                        }
                    }
                });
                acButton.performClick();
            }
            else {
                LinearLayout outdoor = (LinearLayout) getInflater().inflate(R.layout.form_outdoor, viewGroups, false);
                layout.addView(outdoor);
                type = "outdoor";
            }
            ((TextView) findViewById(R.id.textView_type)).setText(Html.fromHtml("<b>Type:" + type + "</b>"));
        }
        Button reviewDocButton = (Button) findViewById(R.id.button_reviewDoc);
        if (reviewDocButton != null) {
            reviewDocButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(done_onclick != null) done_onclick.onClick(v);
                    else Utils.makeToast(getActivity(), "No listener defined");
                }
            });
        }

        baseAsyncTaskLoad = new BaseAsyncTask(getActivity(), "Load...") {
            @Override
            protected Object doInBackground(Object[] params) {
                load(viewGroups, null);
                return null;
            }
        };
        baseAsyncTaskLoad.execute();
    }

    @Override
    public void onResume() {
        super.onResume();
        onResume = true;
        date = Utils.miliSecondToDateString(BaseActivity.getGpsTime());
    }

    @Override
    public String getTagFragment() {
        return "Form Fragment";
    }

    @Override
    public void onStop() {
        super.onStop();
        simpan();
    }

    private void simpan(){
        baseAsyncTaskLoad.cancel(true);
        boolean mandatoryComplete = save(viewGroups, null, null);
        System.out.println("mandatory complete: "+mandatoryComplete);
        if (mandatoryComplete) {
            String mandatory = taskList.getMandatory();
            List<String> mandatoryList = Arrays.asList(mandatory.split(","));
            Log.d("", mandatoryList.toString());
            if (!mandatoryList.contains(PMFormFragment.currentPos+"")) {
                taskList.setMandatory(mandatory + (PMFormFragment.currentPos) + ",");
                Log.d("mandatory ne cak", taskList.getMandatory());
                taskList.saveMerge();
            }
        }
    }

    private void load(ViewGroup viewGroup, String metaName) {
        int count = viewGroup.getChildCount();
        if (getActivity() != null) {
            for (int j = 0; j < count; j++) {
                if (baseAsyncTaskLoad.isCancelled()) break;
                View viewChild = viewGroup.getChildAt(j);
                if (viewChild instanceof TextView && !(viewChild instanceof EditText)) {
                    if (j != count - 1) {
                        metaName = ((TextView) viewChild).getText().toString();
                        if (groupText != null) metaName = groupText + "#" + metaName;
                        metaName = metaName.replaceAll(System.getProperty("line.separator"), "").replaceAll("[^A-Za-z0-9#]", " ").trim().replaceAll("\\s+", "_").toLowerCase();
                    }
                } else if (viewChild instanceof RadioGroup) {
                    Meta meta = Meta.getMeta("" + idPmTerakhir, metaName, "radio");
                    if (meta != null) {
                        RadioGroup radioGroup = (RadioGroup) viewChild;
                        for (int k = 0; k < radioGroup.getChildCount(); k++) {
                            if (radioGroup.getChildAt(k) instanceof RadioButton) {
                                final RadioButton radioButton = (RadioButton) radioGroup.getChildAt(k);
                                if (radioButton.getText().toString().equalsIgnoreCase(meta.getMetaValue())) {
                                    if (baseAsyncTaskLoad.isCancelled()) break;
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            radioButton.setChecked(true);
                                        }
                                    });
                                    break;
                                }
                            }
                        }
                    }
                } else if (viewChild instanceof EditText) {
                    final EditText editText = (EditText) viewChild;
                    if (editText.getTag() != null) {
                        metaName += ("_" + editText.getTag().toString());
                    }
                    final Meta meta = Meta.getMeta("" + idPmTerakhir, metaName, "text");
                    if (meta != null) {
                        if (baseAsyncTaskLoad.isCancelled()) break;
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                editText.setText(meta.getMetaValue());
                            }
                        });
                    }
                } else if (viewChild instanceof Spinner) {
                    Meta meta = Meta.getMeta("" + idPmTerakhir, metaName, "checkbox");
                    if (meta != null) {
                        final Spinner spinner = (Spinner) viewChild;
                        SpinnerAdapter spinnerAdapter = spinner.getAdapter();
                        for (int k = 0; k < spinnerAdapter.getCount(); k++) {
                            if (spinnerAdapter.getItem(k).toString().equalsIgnoreCase(meta.getMetaValue())) {
                                if (baseAsyncTaskLoad.isCancelled()) break;
                                final int a = k;
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        spinner.setSelection(a);
                                    }
                                });
                                break;
                            }
                        }
                    }
                } else if (viewChild instanceof UploadButton) {
                    final Meta meta = Meta.getMeta("" + idPmTerakhir, metaName, "image");
                    if (meta != null) {
                        final UploadButton uploadButton = (UploadButton) viewChild;
                        if (baseAsyncTaskLoad.isCancelled()) break;
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                String[] tag = meta.getMetaValue().split("#");
                                uploadButton.setImageTrulyPath(tag[0]);
                            }
                        });
                    }
                } else if (viewChild instanceof LinearLayout) {
                    LinearLayout layout = (LinearLayout) viewChild;
                    load(layout, metaName);
                }
            }
        }
    }

    public boolean save(ViewGroup viewGroup, String metaName, String fieldName) {
        int count = viewGroup.getChildCount();
        boolean isMandatory = false;
        for (int j = 0; j < count; j++) {
            String metaValue = "";
            String metaInputType = "";
            View viewChild = viewGroup.getChildAt(j);
            if (viewChild instanceof TextView && !(viewChild instanceof EditText)) {
                TextView textView = (TextView) viewChild;
                metaName = textView.getText().toString();
                fieldName = metaName;
                isMandatory = textView.getTag() != null && textView.getTag().equals("1");
                if (groupText != null) metaName = groupText + "#" + metaName;
            } else if (viewChild instanceof RadioGroup) {
                RadioGroup radioGroup = (RadioGroup) viewChild;
                int id = radioGroup.getCheckedRadioButtonId();
                metaInputType = "radio";
                if (id != -1) {
                    RadioButton radioButton = (RadioButton) radioGroup.findViewById(id);
                    metaValue = radioButton.getText().toString().toLowerCase();
                }
            } else if (viewChild instanceof EditText) {
                EditText editText = (EditText) viewChild;
                metaValue = editText.getText().toString();
                metaInputType = "text";
                if (editText.getTag() != null) {
                    metaName += ("_" + editText.getTag().toString());
                }

            } else if (viewChild instanceof Spinner) {
                Spinner spinner = (Spinner) viewChild;
                metaValue = spinner.getSelectedItem().toString().toLowerCase();
                metaInputType = "checkbox";
            } else if (viewChild instanceof UploadButton) {
                UploadButton uploadButton = (UploadButton) viewChild;
                metaValue = uploadButton.getImagePath();
                metaInputType = "image";

            } else if (viewChild instanceof LinearLayout) {
                LinearLayout layout = (LinearLayout) viewChild;
                if (!save(layout, metaName, fieldName)) return false;
            }

            if (!(viewChild.getClass().equals(LinearLayout.class)) && !(viewChild instanceof TextView)
                    && !A.start_camera && !(viewChild.getClass().equals(TextView.class))
                    && isMandatory && Utils.isTextEmpty(metaValue)) {
                new AlertDialog.Builder(getActivity())
                        .setTitle("Gagal")
                        .setMessage("'" + fieldName + "' type '" + metaInputType + "' wajib terisi")
                        .setPositiveButton("Tutup", null)
                        .show();
                view = viewChild;

                return false;
            }
            else if((! TextUtils.isEmpty(metaName)) && (! TextUtils.isEmpty(metaInputType))){
                metaName = metaName.replaceAll(System.getProperty("line.separator"), "").replaceAll("[^A-Za-z0-9#]", " ").trim().replaceAll("\\s+", "_").toLowerCase();
                String watermark;
                if (metaValue != null && metaValue.contains(Constants.FILE_IMAGE)) {
                    watermark = Utils.getWatermarkFromImage(metaValue);
                }
                else watermark = date;

                Meta meta = new Meta(idPmTerakhir, metaName, metaValue, metaInputType, watermark);
                meta.setIsUpload(Delivery.PENDING);
                meta.saveMerge();
            }
        }
        return true;
    }

}
