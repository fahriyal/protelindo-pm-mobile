package id.co.firzil.protelindopmmobile.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.gz.databaselibrary.ServiceHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import id.co.firzil.protelindopmmobile.Constants;
import id.co.firzil.protelindopmmobile.Delivery;
import id.co.firzil.protelindopmmobile.Me;
import id.co.firzil.protelindopmmobile.Tag;
import id.co.firzil.protelindopmmobile.URL;
import id.co.firzil.protelindopmmobile.Utils;
import id.co.firzil.protelindopmmobile.entity.LogLatLong;
import id.co.firzil.protelindopmmobile.entity.Meta;
import id.co.firzil.protelindopmmobile.entity.TaskList;
import id.co.firzil.protelindopmmobile.entity.TaskListOverview;
import id.co.firzil.protelindopmmobile.fragment.PMListFragment;
import id.co.firzil.protelindopmmobile.ui.SyncMetaNotification;

/*
 * Created by riorizkyrainey on 15/10/15.
 */
public class SubmitAnyarService extends Service {

    private Intent responseIntent = new Intent(PMListFragment.INTENT_FILTER);

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        List<TaskList> taskLists = TaskList.submitPendingList();
        System.out.println("TaskList list: " + taskLists);
        for (TaskList taskList : taskLists) {
            if (Utils.isOnline(getBaseContext())) {
                AsyncProcessUpload asyncProcessUpload = new AsyncProcessUpload(getBaseContext(), taskList);
                asyncProcessUpload.execute();
            }
        }
        List<LogLatLong> logLatLongs = LogLatLong.submitPendingList();
        System.out.println("LogLatLong list: " + logLatLongs);
        for (LogLatLong logLatLong : logLatLongs) {
            if (Utils.isOnline(getBaseContext())) {
                AsyncProcessLogUpload asyncProcessUpload = new AsyncProcessLogUpload(getBaseContext(), logLatLong);
                asyncProcessUpload.execute();
            }
        }
        return START_STICKY;
    }

    private class AsyncProcessLogUpload extends AsyncTask<String, String, String> {

        Context context;
        LogLatLong logLatLong;

        AsyncProcessLogUpload(Context context, LogLatLong logLatLong) {
            this.context = context;
            this.logLatLong = logLatLong;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            logLatLong.setHasBeenSubmitted(LogLatLong.SUBMIT_SENDING);
            logLatLong.saveMerge();
        }

        @Override
        protected String doInBackground(String... params) {
            System.out.println(getClass().getSimpleName());
            final String[] status = {"gagal"};
            do {
                if (Utils.isOnline(context)) {
                    try {

                        ServiceHandler logLatLongeHandler = new ServiceHandler(getBaseContext(), false);
                        logLatLongeHandler.addParam("SiteID", logLatLong.getSiteId());
                        logLatLongeHandler.addParam("Type", logLatLong.getType());
                        logLatLongeHandler.addParam("RefNo", logLatLong.getRefNo());
                        logLatLongeHandler.addParam("LatOri", logLatLong.getLatOriginal());
                        logLatLongeHandler.addParam("LongOri", logLatLong.getLongOriginal());
                        logLatLongeHandler.addParam("LatPosisi", logLatLong.getLatPosisi());
                        logLatLongeHandler.addParam("LongPosisi", logLatLong.getLongPosisi());
                        logLatLongeHandler.addParam("Jarak", logLatLong.getJarak());
                        logLatLongeHandler.addParam("ActivityDateTime", logLatLong.getActivityDateTime());
                        logLatLongeHandler.addParam("APKVersion", logLatLong.getApkVersion());
                        logLatLongeHandler.addParam("AndroidVersion", logLatLong.getAndroidVersion());
                        logLatLongeHandler.addParam("ModelNumber", logLatLong.getModelNumber());
                        logLatLongeHandler.addParam("KernelVersion", logLatLong.getKernelVersion());
                        logLatLongeHandler.addParam("BuildNumber", logLatLong.getBuildNumber());
                        logLatLongeHandler.addParam("IMEI", logLatLong.getImei());
                        logLatLongeHandler.addParam("createdBy", logLatLong.getCreatedBy());
                        String response = logLatLongeHandler.makeServiceCall(URL.URL_LOG_LATLONG, ServiceHandler.POST);
                        System.out.println("Response: " + response);
                        JSONObject object = new JSONObject(response);
                        if (object.optInt(Tag.FLAG) == 1) {
                            status[0] = "berhasil";
                            logLatLong.setHasBeenSubmitted(LogLatLong.SUBMIT_SUBMITTED);
                            logLatLong.saveMerge();
                        } else {
                            status[0] = "berhasil";
                            logLatLong.setHasBeenSubmitted(LogLatLong.SUBMIT_FAILED);
                            logLatLong.saveMerge();
                        }
                    } catch (StackOverflowError e) {
                        Log.d("Offline Mode", "masuk stackOverFlow");
                        e.printStackTrace();
                        status[0] = "gagal";
                    } catch (Exception e) {
                        Log.d("Offline Mode", "masuk exception");
                        e.printStackTrace();
                        status[0] = "gagal";
                    }
                }
            } while ("gagal".equalsIgnoreCase(status[0]));
            return "berhasil";
        }

    }

    private class AsyncProcessUpload extends AsyncTask<String, String, String> {

        Context context;
        TaskList taskList;
        SyncMetaNotification metaNotification;

        AsyncProcessUpload(Context context, TaskList taskList) {
            this.context = context;
            this.taskList = taskList;
            metaNotification = new SyncMetaNotification(context, taskList.getIdPm().toString());
            metaNotification.setProgress(2, 0);

            LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(PMListFragment.INTENT_FILTER_UPLOAD_READY));
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            taskList.setSubmissionStatus(TaskList.SUBMIT_SENDING);
            taskList.saveMerge();
            sendOrderedBroadcast(responseIntent, null);
        }

        @Override
        protected String doInBackground(String... params) {
            final String[] status = {"gagal"};
            do {
                if (Utils.isOnline(context)) {
                    try {

                        List<Meta> metaList = Meta.findByIdPM(taskList.getIdPm().toString());
                        int percobaan = 0;
                        int jumlahUpload = 0;

                        for (int i = 0; i < metaList.size(); i++) {
                            Meta meta = metaList.get(i);
                            if (percobaan == 3) { //jika sudah 3 kali upload gagal maka berhenti
                                metaNotification.uploadFailed();
                                break;
                            }
                            if (meta.getIsUpload() != Delivery.SUCCEED) {
                                ServiceHandler serviceHandler = new ServiceHandler(getBaseContext(), false);
                                serviceHandler.addParam(Tag.JENIS, "pm");
                                serviceHandler.addParam(Tag.ID, taskList.getIdPm().toString());
                                serviceHandler.addParam(Tag.META_TYPE, meta.getMetaInputType());
                                serviceHandler.addParam(Tag.DATETIME, meta.getMetaDate());
                                serviceHandler.addParam(Tag.META_NAME, meta.getMetaName());
                                String response;
                                System.out.println("Meta: " + meta.toString());
                                if (meta.getMetaInputType().equals("image")) {
                                    if (meta.getMetaValue().contains(Constants.FILE_IMAGE)) {
                                        response = serviceHandler.uploadImageNoAsync(URL.SAVE_META, Tag.META_VALUE, meta.getMetaValue());
                                    } else {
                                        meta.setIsUpload(Delivery.SUCCEED);
                                        meta.updateStatusUpload(Delivery.SUCCEED);
                                        metaNotification.setProgress(metaList.size(), i);
                                        percobaan = 0;
                                        jumlahUpload++;
                                        continue;
                                    }
                                } else {
                                    serviceHandler.addParam(Tag.META_VALUE, meta.getMetaValue());
                                    response = serviceHandler.makeServiceCall(URL.SAVE_META, ServiceHandler.POST);
                                }

                                if (!Utils.isTextEmpty(response)) {

                                    meta.setIsUpload(Delivery.SUCCEED);
                                    meta.updateStatusUpload(Delivery.SUCCEED);
                                    metaNotification.setProgress(metaList.size(), i);
                                    percobaan = 0;
                                    jumlahUpload++;
                                } else {

                                    i--;
                                    percobaan++;
                                }
                            } else {

                                jumlahUpload++;
                            }
                        }

                        if (jumlahUpload == metaList.size()) { //jika jumlah upload sudah sama
                            metaNotification.loadReportSubmit();
                            ServiceHandler serviceHandlerBlocked = new ServiceHandler(getBaseContext(), false);
                            serviceHandlerBlocked.addParam(Tag.IDTASK, taskList.getIdPm().toString());

                            int ba = taskList.getIsBlockedAccess();
                            if(ba == TaskList.NOT_BLOCKED_ACCESS) ba = 0;

                            serviceHandlerBlocked.addParam(Tag.BLOCKED_ACCESS, ""+ba);
                            serviceHandlerBlocked.addParam(Tag.IDUSER, Me.getValue(getBaseContext(), Me.IDVENDOR_USER));

                            String responseBlocked = serviceHandlerBlocked.makeServiceCall(URL.BLOCKED_ACCESS, ServiceHandler.POST);

                            try {
                                System.out.println(responseBlocked);
                                JSONObject jsonObjectBlock = new JSONObject(responseBlocked);
                                if (jsonObjectBlock.optInt(Tag.FLAG) == 1) {

                                    ServiceHandler serviceHandler = new ServiceHandler(getBaseContext(), false);
                                    serviceHandler.addParam(Tag.IDUSER, Me.getValue(getBaseContext(), Me.IDVENDOR_USER));
                                    serviceHandler.addParam(Tag.IDTASK, "" + taskList.getIdPmPlanDetil());
                                    serviceHandler.addParam(Tag.DATETIME, taskList.getSubmitDate());

                                    String response = serviceHandler.makeServiceCall(URL.SUBMIT_REPORT, ServiceHandler.POST);
                                    try {
                                        System.out.println(response);
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.optInt(Tag.FLAG) == 1) {

                                            TaskListOverview taskListOverviewChange = TaskListOverview.findById(TaskListOverview.class, taskList.getStatus());
                                            if (taskListOverviewChange != null) {
                                                taskListOverviewChange.setCount(taskListOverviewChange.getCount() - 1);
                                                taskListOverviewChange.saveMerge();
                                            }
                                            TaskListOverview taskListOverview = TaskListOverview.findById(TaskListOverview.class, "Need Approval by Vendor");
                                            if (taskListOverview != null) {
                                                taskListOverview.setCount(taskListOverview.getCount() + 1);
                                                taskListOverview.saveMerge();
                                            }
                                            taskList.setStatus("Need Approval by Vendor");

                                            taskList.setSubmissionStatus(TaskList.SUBMIT_SUBMITTED);
                                            metaNotification.reportSubmitted();
                                        }
                                        else if (jsonObject.optInt(Tag.FLAG) == 0) {
                                            taskList.setSubmissionStatus(TaskList.SUBMIT_FAILED);
                                            metaNotification.reportFailed(jsonObject.optString(Tag.MSG));
                                        }
                                        status[0] = "berhasil";
                                    }
                                    catch (JSONException e) {
                                        e.printStackTrace();
                                        status[0] = "gagal";
                                        taskList.setSubmissionStatus(TaskList.SUBMIT_PENDING);
                                    }
                                }
                                else{
                                    taskList.setSubmissionStatus(TaskList.SUBMIT_FAILED);
                                    metaNotification.reportFailed(jsonObjectBlock.optString(Tag.MSG, "Failed set blocked access"));
                                }
                            }
                            catch (JSONException e) {
                                taskList.setSubmissionStatus(TaskList.SUBMIT_PENDING);
                                e.printStackTrace();
                                status[0] = "gagal";
                            }
                        }
                        else {
                            taskList.setSubmissionStatus(TaskList.SUBMIT_PENDING);
                            status[0] = "gagal";
                        }

                    }
                    catch (StackOverflowError | Exception e) {
                        taskList.setSubmissionStatus(TaskList.SUBMIT_FAILED);
                        metaNotification.reportFailed("Error exception or stackoverflowerror\n"+e.getMessage());
                        e.printStackTrace();
                        status[0] = "gagal";
                    }
                    taskList.saveMerge();
                    sendOrderedBroadcast(responseIntent, null);
                }
                else break;
                System.out.println("garaaappp");

            } while ("gagal".equalsIgnoreCase(status[0]));
            return "berhasil";
        }

    }
}
