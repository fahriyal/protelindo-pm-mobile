package id.co.firzil.protelindopmmobile;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiskCache;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class CustomImageLoader {

    private DisplayImageOptions options;
    private ImageLoader imageLoader;

    public CustomImageLoader(Context c) {

        File cacheDir = StorageUtils.getOwnCacheDirectory(c, "PM/cache");//for caching
        imageLoader = ImageLoader.getInstance();

        if (!imageLoader.isInited()) {
            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(c)
                    .diskCache(new UnlimitedDiskCache(cacheDir)) // You can pass your own disc cache implementation
                    .memoryCache(new WeakMemoryCache())
                    .imageDownloader(new AuthImageDownloader(c, 2500, 2500))
//			.diskCacheFileNameGenerator(new HashCodeFileNameGenerator())
                    .build();
            imageLoader.init(config);
        }
        int logo = 0;
        options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(logo)
                .showImageOnFail(logo)
                .considerExifParams(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .cacheInMemory(false)
                .resetViewBeforeLoading(true)
                .cacheOnDisk(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .resetViewBeforeLoading(true)
                .build();
    }

    public void displayImage(String url, ImageView im) {
        try {
            imageLoader.displayImage(url, im, options, new AnimateFirstDisplayListener(0));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void displayImage(String url, ImageView im, View view) {
        try {
            imageLoader.displayImage(url, im, options, new AnimateFirstDisplayListener(view));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        private View pb;
        private int defaultImage;

        public AnimateFirstDisplayListener(int defaultImage) {
            this.defaultImage = defaultImage;
        }

        public AnimateFirstDisplayListener(View pb) {
            this.pb = pb;
        }

        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 1400);
                    displayedImages.add(imageUri);
                }
                System.out.println("LOADED IMAGE");
            }
            if (pb != null) {
                pb.setVisibility(View.GONE);
                System.out.println("progressbar gone");
            }
        }

        public void onLoadingStarted(String imageUri, View view) {
            if (pb != null) pb.setVisibility(View.VISIBLE);
        }

        public void onLoadingCancelled(String imageUri, View view) {

        }

        @Override
        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            if (defaultImage != 0) {
                ((ImageView) view).setImageResource(defaultImage);
            }
        }

    }

}
