package id.co.firzil.protelindopmmobile.service;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import id.co.firzil.protelindopmmobile.FcmPreference;
import id.co.firzil.protelindopmmobile.Me;
import id.co.firzil.protelindopmmobile.UpdateRegid;

/*
 * Created by Fahriyal Afif on 8/17/2016.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        String regid = FirebaseInstanceId.getInstance().getToken();
        if(regid == null) regid = "";
        System.out.println("Refreshed token: " + regid);

        new FcmPreference(this).setIsRegisteredInServer(false);
        Me.commit(this, Me.FCM_REGID, regid);

        new UpdateRegid(this).updateSync();

    }
}
