package id.co.firzil.protelindopmmobile.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.gz.databaselibrary.ConnectionListener;
import com.gz.databaselibrary.ServiceHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import id.co.firzil.protelindopmmobile.BaseAdapter;
import id.co.firzil.protelindopmmobile.Delivery;
import id.co.firzil.protelindopmmobile.Dialog;
import id.co.firzil.protelindopmmobile.EndlessScroll;
import id.co.firzil.protelindopmmobile.EndlessScrollListener;
import id.co.firzil.protelindopmmobile.GPSUtils;
import id.co.firzil.protelindopmmobile.Me;
import id.co.firzil.protelindopmmobile.PreparingUpload;
import id.co.firzil.protelindopmmobile.R;
import id.co.firzil.protelindopmmobile.Tag;
import id.co.firzil.protelindopmmobile.URL;
import id.co.firzil.protelindopmmobile.Utils;
import id.co.firzil.protelindopmmobile.entity.LogLatLong;
import id.co.firzil.protelindopmmobile.entity.Meta;
import id.co.firzil.protelindopmmobile.entity.TaskList;
import id.co.firzil.protelindopmmobile.entity.Tenant;
import id.co.firzil.protelindopmmobile.service.SubmitAnyarService;
import id.co.firzil.protelindopmmobile.ui.GreenButton;

public class PMListFragment extends BaseActivity implements EndlessScrollListener{

    public static final String INTENT_FILTER = "id.co.firzil.protelindopmmobile.task_list",
            INTENT_FILTER_UPLOAD_READY = "id.co.firzil.protelindopmmobile.task_list.upload_ready";

    private String pmStatus;
    private int pmPeriode;
    private EditText searchEditText;
    private PMOverviewAdapter troubleTicketAdapter;
    private boolean masukForm = false;
    private ProgressDialog dialogPreparingUpload;

    private BroadcastReceiver submitReportReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            System.out.println("masuk receiver");
            loadTask(false);
        }
    }, uploadReadyReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent in) {
            System.out.println("Masuk on ceiveii");
            if(in.getAction().equalsIgnoreCase(INTENT_FILTER_UPLOAD_READY)){
                System.out.println("Masuk on ceiveii : dismiss");
                try{
                    dialogPreparingUpload.dismiss();
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
            else System.out.println("Masuk on ceiveii : else");
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        pmStatus = getIntent().getExtras().getString("pmstatus");
        pmPeriode = getIntent().getExtras().getInt("periode");
        String keyword = getIntent().getStringExtra("keyword");

        setContentView(R.layout.fragment_pmlist);
        setNavbar();
        String TAG = "PM List";
        setTitle(TAG);
        ListView listView = (ListView) findViewById(R.id.listView);
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        searchEditText = (EditText) findViewById(R.id.editText_search);
        spinner.setAdapter(ArrayAdapter.createFromResource(c, R.array.search_list, android.R.layout.simple_spinner_item));
        troubleTicketAdapter = new PMOverviewAdapter(c);
        listView.setAdapter(troubleTicketAdapter);
        EndlessScroll endlessScroll = new EndlessScroll();
        endlessScroll.setEndlessScrollListener(this);
        listView.setOnScrollListener(endlessScroll);
        Button search = (Button) findViewById(R.id.button_cari);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadTask(true);
            }
        });

        if(savedInstanceState != null){
            System.out.println("NGELUUUUUU");
            pmPeriode = savedInstanceState.getInt("pmPeriode");
            pmStatus = savedInstanceState.getString("pmStatus");
            keyword = savedInstanceState.getString("keyword");
        }

        if(keyword == null) keyword = "";
        searchEditText.setText(keyword);

        troubleTicketAdapter = new PMOverviewAdapter(c);
        listView.setAdapter(troubleTicketAdapter);

        LocalBroadcastManager.getInstance(c).registerReceiver(uploadReadyReceiver, new IntentFilter(INTENT_FILTER_UPLOAD_READY));
        registerReceiver(submitReportReceiver, new IntentFilter(INTENT_FILTER));

        loadTask(false);
    }

    private void reloadFromDevice(){
        List<TaskList> listOverviews = TaskList.findByPmpStatus(TaskList.class, pmStatus, pmPeriode, searchEditText.getText().toString());
        troubleTicketAdapter.clear();
        troubleTicketAdapter.add(listOverviews);
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (masukForm) {
                System.out.println("REFRESH ABIS MASUK FORM");
                reloadFromDevice();
                masukForm = false;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void onDestroy(){
        super.onDestroy();
        LocalBroadcastManager.getInstance(c).unregisterReceiver(uploadReadyReceiver);
        unregisterReceiver(submitReportReceiver);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.main:
                loadTask(true);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("pmPeriode", pmPeriode);
        outState.putString("pmStatus", pmStatus);
        outState.putString("keyword", searchEditText.getText().toString());
    }

    private void loadTask(final boolean refresh) {

        reloadFromDevice();

        if (Utils.isOnline(c)) {
            ServiceHandler serviceHandler;
            if (!TaskList.isHasData(pmStatus) || refresh) {
                serviceHandler = new ServiceHandler(c, getString(R.string.tunggu));
            } else {
                serviceHandler = new ServiceHandler(c, false);
            }
            serviceHandler.addParam(Tag.IDUSER, Me.getValue(c, Me.IDVENDOR_USER));
            serviceHandler.addParam(Tag.START, "0");
            serviceHandler.addParam(Tag.OFFSET, LIMIT);
            serviceHandler.addParam(Tag.STATUS, pmStatus);
            serviceHandler.addParam(Tag.KEYWORD, searchEditText.getText().toString());
            serviceHandler.addParam(Tag.PENDING_REVIEW, pmPeriode + "");
            serviceHandler.setConnectionListener(new ConnectionListener() {
                @Override
                public void onBeforeRequest() {
                }

                @Override
                public void onAfterRequested(String response) {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (jsonObject != null) {
                        if (jsonObject.optInt(Tag.FLAG) == 1) {
                            JSONArray jsonArray = null;
                            try {
                                jsonArray = jsonObject.getJSONArray(Tag.RESULT);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            if (jsonArray != null) {
                                String in[] = new String[jsonArray.length()];
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    TaskList taskList = null;
                                    JSONObject object = null;
                                    String idpm = "";
                                    int blocked = 0;
                                    try {
                                        object = jsonArray.getJSONObject(i);
                                        idpm = object.getString("idpm");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    if (object != null) {
                                        taskList = TaskList.findById(TaskList.class, idpm);
                                        if(taskList == null) {
                                            System.out.println("tasklist e null");
                                            taskList = new TaskList();
                                        }
                                        else System.out.println("tasklist e OKEE");
                                        blocked = taskList.getIsBlockedAccess();
                                        System.out.println("tasklist e blocked: "+blocked);

                                        taskList.initFromJSONObject(object);

                                        JSONObject jsonObjectInfoAlt = null;
                                        try {
                                            jsonObjectInfoAlt = object.getJSONObject("generalinfo_alt");
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        if (jsonObjectInfoAlt != null) {
                                            System.out.println("jsonObjectInfoAlt");
                                            try {
                                                taskList.setRadius(jsonObjectInfoAlt.getDouble(Tag.RADIUS));
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            try {
                                                System.out.println("latitude");
                                                taskList.setLatitude(jsonObjectInfoAlt.getString("latitude"));
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            try {
                                                System.out.println("longitude");
                                                taskList.setLongitude(jsonObjectInfoAlt.getString("longitude"));
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                    System.out.println("tasklist.setPeriode");
                                    taskList.setPeriode(pmPeriode);
                                    taskList.setIsBlockedAccess(blocked);
                                    taskList.saveMerge(); //save ke db ketika tidak ada, update ketika sudah ada.

                                    in[i] = taskList.getIdPm()+"";

                                }
                                TaskList.kosongkan(TaskList.class, pmStatus, pmPeriode, searchEditText.getText().toString(), in);
                            }
                        }
                        reloadFromDevice();
                    }
                }
            });
            addServiceHandler(serviceHandler);
            serviceHandler.makeServiceCallAsyncTask(URL.MY_TASK_LIST, ServiceHandler.GET);
        }
    }

    private static final String LIMIT = 999999+"";
    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        /*final List<TaskList> listOverviewsMore = new ArrayList<>();
        ServiceHandler serviceHandler = new ServiceHandler(c, "Load More...");
        serviceHandler.addParam(Tag.IDUSER, Me.getValue(c, Me.IDVENDOR_USER));
        serviceHandler.addParam(Tag.START, "1");
        serviceHandler.addParam(Tag.OFFSET, LIMIT);
        serviceHandler.addParam(Tag.STATUS, pmStatus);
        serviceHandler.addParam(Tag.PENDING_REVIEW, pmPeriode + "");
        serviceHandler.setConnectionListener(new ConnectionListener() {
            @Override
            public void onBeforeRequest() {
            }

            @Override
            public void onAfterRequested(String response) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (jsonObject != null) {
                    if (jsonObject.optInt(Tag.FLAG) == 1) {
                        JSONArray jsonArray = null;
                        try {
                            jsonArray = jsonObject.getJSONArray(Tag.RESULT);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (jsonArray != null) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                TaskList taskList = new TaskList();
                                JSONObject object = null;
                                try {
                                    object = jsonArray.getJSONObject(i);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                if (object != null) {
                                    taskList.initFromJSONObject(object);

                                    JSONObject jsonObjectInfoAlt = null;
                                    try {
                                        jsonObjectInfoAlt = object.getJSONObject("generalinfo_alt");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    if (jsonObjectInfoAlt != null) {
                                        System.out.println("jsonObjectInfoAlt");
                                        try {
                                            taskList.setRadius(jsonObjectInfoAlt.getDouble(Tag.RADIUS));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            System.out.println("latitude");
                                            taskList.setLatitude(jsonObjectInfoAlt.getString("latitude"));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            System.out.println("longitude");
                                            taskList.setLongitude(jsonObjectInfoAlt.getString("longitude"));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                                System.out.println("tasklist.setPeriode");
                                taskList.setPeriode(pmPeriode);
                                taskList.saveMerge(); //save ke db ketika tidak ada, update ketika sudah ada.
                                listOverviewsMore.add(taskList);
                            }
                        }
                    }
                    troubleTicketAdapter.add(listOverviewsMore);
                }
            }
        });
        addServiceHandler(serviceHandler);
        serviceHandler.makeServiceCallAsyncTask(URL.MY_TASK_LIST, ServiceHandler.GET);*/
    }

    public class PMOverviewAdapter extends BaseAdapter<TaskList> {

        private View.OnClickListener notAllowedListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int position = (int) v.getTag();
                TaskList taskList = getItem(position);
                System.out.println("idPM: " + taskList.getIdPm());
                Utils.makeToast(c, "Fill report dan submit report tidak diperbolehkan");
            }
        };
        private View.OnClickListener viewMapListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (int) v.getTag();
                Intent intent = new Intent(c, PMMapFragment.class);
                intent.putExtra("lat", Double.parseDouble(getItem(position).getLatitude()));
                intent.putExtra("long", Double.parseDouble(getItem(position).getLongitude()));
                intent.putExtra("name", getItem(position).getName());
                intent.putExtra("powerType", getItem(position).getPowerType());
                intent.putExtra("tasklist", getItem(position));
                startActivity(intent);
            }
        };
        private View.OnClickListener submitReportListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (int) v.getTag();
                final TaskList taskList = getItem(position);
                System.out.println("idPM: " + taskList.getIdPm());
                if (taskList.getSubmissionStatus() == TaskList.SUBMIT_NOT_YET || taskList.getSubmissionStatus() == TaskList.SUBMIT_FAILED
                        || taskList.getSubmissionStatus() == TaskList.SUBMIT_SUBMITTED) {
                    if (GPSUtils.isGPSEnabled(c)) {
                        if (getGpsTime) {
                            submitReport(taskList);
                        } else {
                            dialogTime = new Dialog(c, "Tunggu...", "Mencari waktu berdasarkan lokasi..");
                            dialogTime.setCancelable(false);
                            dialogTime.setOnDismiss(new Dialog.OnDismiss() {
                                @Override
                                public void onDismiss() {
                                    submitReport(taskList);
                                }
                            });
                            dialogTime.show();
                        }
                    }
                    else GPSUtils.showDialogEnableGPS(c);
                }
                else {
                    Utils.makeToast(c, "Sedang proses submit. Harap tunggu....");
                }
            }
        };
        private View.OnClickListener fillReportListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int position = (int) v.getTag();
                final TaskList taskList = getItem(position);

                if (taskList.getSubmissionStatus() == TaskList.SUBMIT_NOT_YET || taskList.getSubmissionStatus() == TaskList.SUBMIT_FAILED
                        || taskList.getSubmissionStatus() == TaskList.SUBMIT_SUBMITTED){
                    if (GPSUtils.isGPSEnabled(c)) {
                        if (getGpsTime) {
                            Double longitude = lon;
                            Double latitude = lat;

                            double latitudeTower = Double.parseDouble(taskList.getLatitude());
                            double longitudeTower = Double.parseDouble(taskList.getLongitude());

                            float[] meter = new float[3];
                            Location.distanceBetween(latitude, longitude, latitudeTower, longitudeTower, meter);

                            TimeZone tz = TimeZone.getTimeZone("GMT+7");
                            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy kk:mm:ss");
                            sdf.setTimeZone(tz);
                            String date = sdf.format(new Date().getTime());
                            LogLatLong logLatLong = new LogLatLong(c,
                                    taskList.getIdSite(),
                                    "PM",
                                    taskList.getIdPm().toString(),
                                    taskList.getLatitude(),
                                    taskList.getLongitude(),
                                    latitude.toString(),
                                    longitude.toString(),
                                    "" + meter[0],
                                    date);
                            System.out.println(logLatLong.toString());
                            logLatLong.save();

                            startService(new Intent(c, SubmitAnyarService.class));

                            double radius = Double.parseDouble(Me.getValue(c, "radius_tolerance"));

                            if (taskList.getRadius() != 0) {
                                radius = taskList.getRadius();
                            }

                            if (Utils.checkAutoTime(c)) {
                                if (meter[0] < radius) {
                                    if (Utils.isOnline(c)) {
                                        ServiceHandler serviceHandler;
                                        if (!Meta.isHasData("" + taskList.getIdPm())) {
                                            serviceHandler = new ServiceHandler(c, "Loading.. Download meta data from server..");
                                            serviceHandler.addParam(Tag.TYPE, "pm");
                                            serviceHandler.addParam(Tag.ID, "" + taskList.getIdPm());
                                            serviceHandler.setConnectionListener(new ConnectionListener() {
                                                @Override
                                                public void onBeforeRequest() {
                                                    if (Meta.isHasData("" + taskList.getIdPm())) {
                                                        goNextStep(position);
                                                    }
                                                }

                                                @Override
                                                public void onAfterRequested(String response) {
                                                    if (response != null) {
                                                        try {
                                                            JSONObject jsonObject = new JSONObject(response);
                                                            if (jsonObject.optInt(Tag.FLAG) == 1) {
                                                                JSONArray jsonArray = jsonObject.getJSONArray(Tag.DATA_META);
                                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                                    JSONObject object = jsonArray.getJSONObject(i);
                                                                    Meta meta = new Meta();
                                                                    meta.initFromJSONObject(object);
                                                                    meta.setIdMeta(null);
                                                                    meta.setIsUpload(Delivery.SUCCEED);
                                                                    meta.saveMerge();
                                                                }
                                                            }
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                    goNextStep(position);
                                                }
                                            });
                                            serviceHandler.makeServiceCallAsyncTask(URL.GET_ALL_META, ServiceHandler.GET);
                                        }
                                        else goNextStep(position);
                                    }
                                    else goNextStep(position);
                                }
                                else {
                                    Dialog dialoga = new Dialog(c, "Error Lokasi", "---------------------------------------\n" +
                                            "WARNING LOCATION\n" +
                                            "---------------------------\n" +
                                            "Posisi anda:\n" +
                                            "lat:" + latitude + "\n" +
                                            "long: " + longitude + "\n" +
                                            "---------------------------\n" +
                                            "Posisi tower:\n" +
                                            "lat: " + latitudeTower + "\n" +
                                            "long: " + longitudeTower + "\n" +
                                            "---------------------------\n" +
                                            "Jarak: " + meter[0] + " meter");
                                    dialoga.setTrueButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                                    dialoga.show();
                                }
                            }
                        } 
                        else GPSUtils.showDialogUnknownLocation(c);
                    }
                    else GPSUtils.showDialogEnableGPS(c);
                }
                else Utils.makeToast(c, "Sedang proses submit. Harap tunggu....");

            }
        };

        public void clear(){
            list.clear();
            notifyDataSetChanged();
        }

        public PMOverviewAdapter(Context context) {
            super(context);
        }

        public void submitReport(TaskList taskList) {
            if (Meta.isSelfieExist("" + taskList.getIdPm())) {
                if(taskList.getIsBlockedAccess() == TaskList.BLOCKED_ACCESS){   //jika blocked access
                    if(dialogPreparingUpload == null) dialogPreparingUpload = new ProgressDialog(c);
                    new PreparingUpload(c, taskList, dialogPreparingUpload).execute();
                }
                else{  //berarti bukan blocked access
                    List<String> mandatoryPage = Arrays.asList(taskList.getMandatory().split(","));

                    System.out.println("mandatory page: " + mandatoryPage);

                    int tenantSize = Tenant.findBySiteId(taskList.getIdSite()).size();
                    boolean terisiSemua = true;

                    for (int i = 0; i < 8 + tenantSize; i++) {
                        if (! mandatoryPage.contains("" + i)) {
                            terisiSemua = false;
                            new AlertDialog.Builder(c)
                                    .setTitle("Gagal")
                                    .setMessage("Data wajib di halaman "+(i + 1)+" belum terisi")
                                    .setPositiveButton("Tutup", null)
                                    .show();
                            break;
                        }
                    }
                    if (terisiSemua) {
                        if(dialogPreparingUpload == null) dialogPreparingUpload = new ProgressDialog(c);
                        new PreparingUpload(c, taskList, dialogPreparingUpload).execute();
                    }
                }
            }
            else {
                if (pmStatus.contains("Reject")) {
                    Utils.makeToast(c, "Report belum diperbaiki");
                } else {
                    Utils.makeToast(c, "Anda belum melakukan selfie");
                }
            }
        }

        private void goNextStep(int position) {
            TaskList taskList = getItem(position);
            boolean selfieExist = Meta.isSelfieExist("" + taskList.getIdPm());
            masukForm = true;

            Class<?> activity;
            if (selfieExist && taskList.getSubmissionStatus() == 1) {
                activity = PMFormFragment.class;
            }
            else if (selfieExist && taskList.getSubmissionStatus() == 0) {
                activity = PreBlockedAccessFragment.class;
            }
            else {
                activity = SelfieFragment.class;
            }
            Intent intent = new Intent(c, activity);
            intent.putExtra("taskList", taskList);
            startActivity(intent);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = getInflater().inflate(R.layout.sample_card_list_pm, parent, false);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            final TaskList taskList = getItem(position);
            holder.pmPlanTextView.setText(taskList.getPmPlan());
            holder.siteIdTextView.setText(taskList.getIdSite());
            holder.siteNameTextView.setText(taskList.getName().replaceAll(System.getProperty("line.separator"), ""));
            holder.endDateTextView.setText(Utils.changeFormatDate(/*"Jan 1 2015 12:00:00:000 AM"*/taskList.getVendorDeadlineDate(), "MMM dd yyyy hh:mm:ss:S a", "MMM dd yyyy kk:mm") + " WIB");
            holder.statusTextView.setText(taskList.getStatus());
            holder.viewMapButton.setTag(position);
            holder.fillReportButton.setTag(position);
            holder.submitReportButton.setTag(position);
            holder.viewMapButton.setOnClickListener(viewMapListener);
            if (pmStatus.contains("Need") || pmStatus.contains("Approve")) {
                holder.fillReportButton.setOnClickListener(notAllowedListener);
                holder.submitReportButton.setOnClickListener(notAllowedListener);
            } else {
                holder.fillReportButton.setOnClickListener(fillReportListener);
                holder.submitReportButton.setOnClickListener(submitReportListener);
            }
            holder.historyTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(c, StatusHistoryFragment.class);
                    intent.putExtra("taskList", taskList);
                    startActivity(intent);
                }
            });
            return convertView;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        }

        class ViewHolder {

            GreenButton viewMapButton, fillReportButton, submitReportButton;
            TextView pmPlanTextView, siteIdTextView, siteNameTextView, endDateTextView, statusTextView, historyTextView;

            ViewHolder(View itemView) {
                viewMapButton = (GreenButton) itemView.findViewById(R.id.button_viewMap);
                fillReportButton = (GreenButton) itemView.findViewById(R.id.button_fillReport);
                submitReportButton = (GreenButton) itemView.findViewById(R.id.button_submitReport);
                pmPlanTextView = (TextView) itemView.findViewById(R.id.textView_pmPlan);
                siteIdTextView = (TextView) itemView.findViewById(R.id.textView_siteId);
                siteNameTextView = (TextView) itemView.findViewById(R.id.textView_siteName);
                endDateTextView = (TextView) itemView.findViewById(R.id.textView_endDate);
                statusTextView = (TextView) itemView.findViewById(R.id.textView_status);
                historyTextView = (TextView) itemView.findViewById(R.id.textView_statusHistory);
            }
        }


    }
}
