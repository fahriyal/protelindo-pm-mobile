package id.co.firzil.protelindopmmobile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import id.co.firzil.protelindopmmobile.entity.TaskList;
import id.co.firzil.protelindopmmobile.fragment.BaseActivity;
import id.co.firzil.protelindopmmobile.service.SubmitAnyarService;

/**
 * Created by fahriyalafif on 3/25/2016.
 */
public class PreparingUpload extends AsyncTask<String, String, String> {
    private TaskList taskList;
    private Context c;

    public PreparingUpload(Context c, TaskList taskList, ProgressDialog pd){
        this.taskList = taskList;
        this.c = c;
        pd.setCanceledOnTouchOutside(false);
        pd.setCancelable(false);
        pd.setMessage("Please wait...preparing for upload process");

        pd.show();
    }

    @Override
    protected String doInBackground(String... params) {
        taskList.setSubmissionStatus(TaskList.SUBMIT_PENDING);
        taskList.setSubmitDate(Utils.miliSecondToDateString(BaseActivity.getGpsTime()));
        taskList.saveMerge();

        c.startService(new Intent(c, SubmitAnyarService.class));

        return null;
    }
}
