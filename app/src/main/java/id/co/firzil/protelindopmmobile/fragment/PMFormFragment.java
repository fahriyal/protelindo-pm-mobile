package id.co.firzil.protelindopmmobile.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.gz.databaselibrary.ConnectionListener;
import com.gz.databaselibrary.ServiceHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import id.co.firzil.protelindopmmobile.A;
import id.co.firzil.protelindopmmobile.BaseAdapter;
import id.co.firzil.protelindopmmobile.ImageCompressionAsyncTask;
import id.co.firzil.protelindopmmobile.R;
import id.co.firzil.protelindopmmobile.Tag;
import id.co.firzil.protelindopmmobile.URL;
import id.co.firzil.protelindopmmobile.Utils;
import id.co.firzil.protelindopmmobile.entity.Meta;
import id.co.firzil.protelindopmmobile.entity.TaskList;
import id.co.firzil.protelindopmmobile.entity.Tenant;
import id.co.firzil.protelindopmmobile.ui.Alert;

/**
 * Created by Rio Rizky Rainey on 26/02/2015.
 * rizkyrainey@gmail.com
 */
public class PMFormFragment extends BaseActivity  {

    public List<FormFragment> formFragments;
    public static int currentPos;
    private TaskList taskList;
    public final String TAG = "PM Form";
    private LinearLayout linearLayout;
    private EditText jumpToPageEditText;
    private TextView textPageTextView;
    private TextView padlockTextView;
    private TextView totalTenantTextView;
    private ScrollView scrollView;
    private LinearLayout linear;
    private String date;

    private List<Tenant> tenantList;
    private static final int CHANGE_PADLOCK_REQUEST = 9090;
    private Button nextButton;
    private TenantAdapter tenantAdapter;
    private View.OnClickListener done_onclick = new View.OnClickListener(){

        @Override
        public void onClick(View v) {
            finish();
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        taskList = (TaskList) getIntent().getExtras().getSerializable("taskList");

        /*//percobaan get meta name
        List<Meta> metaList = Meta.findByIdPM(taskList.getIdPm().toString());
        for (int i = 0; i < metaList.size(); i++) {
            Meta meta = metaList.get(i);
            System.out.println("METAK NAME = "+meta.getMetaName());
        }
        // endd*/

        setContentView(R.layout.fragment_pm_form);
        setNavbar();
        setTitle(TAG);
        date = Utils.miliSecondToDateString(BaseActivity.getGpsTime());
        scrollView = (ScrollView) findViewById(R.id.scrollView2);
        scrollView.setSmoothScrollingEnabled(true);

        formFragments = new ArrayList<>();
        tenantList = new ArrayList<>();
        tenantAdapter = new TenantAdapter(c);
        tenantAdapter.add(tenantList);
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout_tenat);
        padlockTextView = (TextView) findViewById(R.id.textView_padlock);

        textPageTextView = (TextView) findViewById(R.id.textView_page);
        totalTenantTextView = (TextView) findViewById(R.id.textView_tenant);
        Button prevButton = (Button) findViewById(R.id.button_prev);
        nextButton = (Button) findViewById(R.id.button_next);

        jumpToPageEditText = (EditText) findViewById(R.id.editText_jumpToPage);

        padlockTextView.setText("Current Padlock : ");
        for (int i = 0; i < tenantAdapter.getCount() + 8; i++) {
            FormFragment formFragment = new FormFragment();
            formFragment.setTenantCount(tenantAdapter.getCount());
            formFragment.setIdPm(taskList.getIdPm());
            formFragment.setDate(date);
            formFragment.setIdLayout(i);
            formFragment.setTaskList(taskList);
            formFragment.setDoneOnClickListener(done_onclick);
            if (i > 4 && i < tenantAdapter.getCount() + 8 - 4) {
                formFragment.setGroupText(tenantAdapter.getItem(i - 5).getIdClient());
                formFragment.setTenant(tenantAdapter.getItem(i - 5));
            }
            formFragments.add(formFragment);
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.pager, formFragments.get(0)).commit();
        currentPos = 0;
        textPageTextView.setText("Page " + (currentPos + 1) + " of " + formFragments.size());

        getTenant();

        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentPos > 0) {
                    FormFragment formFragment = formFragments.get(currentPos);
                    formFragment.save(formFragment.getRootView(), null, null);
                    currentPos--;
                    changeForm();
                }
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(currentPos < formFragments.size() - 1) {
                    if (checkMandatory()) {

                        String mandatory = taskList.getMandatory();
                        List<String> mandatoryList = Arrays.asList(mandatory.split(","));
                        Log.d(getClass().getName(), mandatoryList.toString());
                        if (! mandatoryList.contains(currentPos + "")) {
                            taskList.setMandatory(mandatory + currentPos + ",");
                            taskList.saveMerge();
                        }

                        currentPos++;
                        changeForm();
                    }
                }
                else finish();
            }
        });

        findViewById(R.id.button_jumpToPage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = jumpToPageEditText.getText().toString();
                loncat(text);
            }
        });

        Button update = (Button) findViewById(R.id.button_tenant);
        update.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (linearLayout.getChildCount() == 1)
                    addViewTenant();
                else
                    removeViewTenant();
            }
        });

        Button changePadlock = (Button) findViewById(R.id.change);
        changePadlock.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent i = new Intent(c, ChangePadlockFragment.class);
                i.putExtra("taskList", taskList);

                startActivityForResult(i, CHANGE_PADLOCK_REQUEST);
            }
        });

        ((TextView) findViewById(R.id.textView_siteId)).setText(taskList.getIdSite());
        ((TextView) findViewById(R.id.textView_region)).setText(taskList.getRegion());
        ((TextView) findViewById(R.id.textView_province)).setText(taskList.getProvince());
        ((TextView) findViewById(R.id.textView_city)).setText(taskList.getCity());
        ((TextView) findViewById(R.id.textView_address)).setText(taskList.getAddress());
        ((TextView) findViewById(R.id.textView_longitude)).setText(taskList.getLongitude());
        ((TextView) findViewById(R.id.textView_lattitude)).setText(taskList.getLatitude());
        ((TextView) findViewById(R.id.textView_siteHeight)).setText(taskList.getSiteHeight());
        ((TextView) findViewById(R.id.textView_towerType)).setText(taskList.getPowerType());
        ((TextView) findViewById(R.id.textView_anchorTenant)).setText(taskList.getAnchor());
        ((TextView) findViewById(R.id.textView_vendorName)).setText(taskList.getOmSubcount());
        getPadlock();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                if(requestCode == Tag.REQUEST_CODE_CAMERA) {
                    String fileLocation = A.get().getButton().getTempPath();
                    A.get().getButton().setImageTrulyPath(fileLocation);
                    new ImageCompressionAsyncTask(this, new ImageCompressionAsyncTask.Callback() {
                        @Override
                        public void onComplete(String filename) {
                            Meta meta = Meta.findByPathImage(filename);
                            if (meta != null) {
                                String watermark = Utils.getWatermarkFromImage(filename);
                                meta.updateMetaDate(watermark);
                            }
                        }
                    }).execute(fileLocation);
                }
                else if(requestCode == CHANGE_PADLOCK_REQUEST){
                    if(data != null) {
                        taskList.setNumberPadlock(data.getStringExtra("padlock_baru"));
                        padlockTextView.setText("Current Padlock : " + taskList.getNumberPadlock());
                    }
                }
            }
        }
        catch(OutOfMemoryError o){
            o.printStackTrace();
            Alert.show(this, "Sorry, Out Of Memory Error", o.getMessage(), null);
        }
        catch(NullPointerException o){
            o.printStackTrace();
            Alert.show(this, "Sorry, Null Pointer Exception", "Unable to complete previous operation\ndue to low memory", null);
        } catch (Exception o) {
            o.printStackTrace();
            Alert.show(this, "Sorry, Error occured", "Please reload page", null);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(1, R.id.restart_gps, 1, getString(R.string.action_restart_gps)).setEnabled(true);

        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        date = Utils.miliSecondToDateString(getGpsTime());
        A.start_camera = false;
    }

    public boolean checkMandatory() {
        ProgressDialog progressDialog = new ProgressDialog(c);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Tunggu");
        progressDialog.show();
        FormFragment formFragment = formFragments.get(currentPos);
        boolean is = formFragment.save(formFragment.getRootView(), null, null);
        progressDialog.dismiss();
        if (!is) scrollTo(formFragment.view);
        return is;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("taskList", taskList);
    }

    private void changeForm() {
        if(currentPos < formFragments.size()) {
            getSupportFragmentManager().beginTransaction().replace(R.id.pager, formFragments.get(currentPos)).commit();
            textPageTextView.setText("Page " + (currentPos + 1) + " of " + formFragments.size());

            if(currentPos == formFragments.size() - 1) nextButton.setText("End");
            else nextButton.setText("Next");
        }
    }

    private void getPadlock() {
        padlockTextView.setText("Current Padlock : " + taskList.getNumberPadlock());
        if (Utils.isOnline(c)) {
            ServiceHandler serviceHandler;
            if (Utils.isTextEmpty(taskList.getNumberPadlock())) {
                serviceHandler = new ServiceHandler(c, "Get Padlock.. Please wait");
            } else {
                serviceHandler = new ServiceHandler(c, false);
            }
            serviceHandler.addParam(Tag.IDSITE, taskList.getIdSite());
            serviceHandler.setConnectionListener(new ConnectionListener() {
                @Override
                public void onBeforeRequest() {
                }

                @Override
                public void onAfterRequested(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.optInt(Tag.FLAG) == 1) {
                            JSONArray jsonArray = jsonObject.getJSONArray(Tag.RESULT);
                            if (jsonArray.length() > 0) {
                                JSONObject object = jsonArray.getJSONObject(0);
                                String padlock = object.optString(Tag.GATE_ID, "");
                                padlockTextView.setText("Current Padlock : " + padlock);
                                taskList.setNumberPadlock(padlock);
                                taskList.saveMerge();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            serviceHandler.makeServiceCallAsyncTask(URL.PADLOCK_COMBINATION, ServiceHandler.GET);
        }
    }

    private void loncat(String text){
        if (!Utils.isTextEmpty(text)) {
            int page = Integer.parseInt(text);
            if (page < 1 || page > formFragments.size()) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(c);
                alertDialogBuilder.setMessage("page mulai dari 1 hingga " + formFragments.size());
                alertDialogBuilder.setTitle("Kesalahan input");
                alertDialogBuilder.show();
            } else {
                currentPos = page - 1;
                changeForm();
            }
        }
    }

    private void getTenant() {
        tenantList = Tenant.findBySiteId(taskList.getIdSite());
        System.out.println("Jumlah tenant: " + tenantList.size());
        if (Utils.isOnline(c)) {
            ServiceHandler serviceHandler;
            if (Tenant.isHasData(taskList.getIdSite())) {
                serviceHandler = new ServiceHandler(c, false);
            } else {
                serviceHandler = new ServiceHandler(c, "Get Tenant");
            }
            serviceHandler.addParam(Tag.IDSITE, taskList.getIdSite());
            serviceHandler.setConnectionListener(new ConnectionListener() {
                @Override
                public void onBeforeRequest() {
                }

                @Override
                public void onAfterRequested(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.optInt(Tag.FLAG) == 1) {
                            JSONArray jsonArray = jsonObject.getJSONArray(Tag.RESULT);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                Tenant tenant = new Tenant();
                                tenant.initFromJSONObject(object);
                                tenant.setIdTenant(null);
                                tenant.saveMerge();
                                tenantList.add(tenant);
                            }
                            tenantAdapter.add(tenantList);
                            int size = 8 + tenantAdapter.getCount();
                            FormFragment[] tenantFragment = new FormFragment[tenantAdapter.getCount()];
                            for (int i = 0; i < tenantFragment.length; i++) {
                                tenantFragment[i] = new FormFragment();
                                tenantFragment[i].setTenantCount(tenantAdapter.getCount());
                                tenantFragment[i].setIdPm(taskList.getIdPm());
                                tenantFragment[i].setDate(date);
                                tenantFragment[i].setIdLayout(i + 5);
                                tenantFragment[i].setTenant(tenantList.get(i));
                                tenantFragment[i].setTaskList(taskList);
                            }
                            formFragments.addAll(5, Arrays.asList(tenantFragment));
                            for (int i = 0; i < size; i++) {
                                FormFragment formFragment = formFragments.get(i);
                                formFragment.setTenantCount(tenantAdapter.getCount());
                                formFragment.setIdPm(taskList.getIdPm());
                                formFragment.setDate(date);
                                formFragment.setIdLayout(i);
                                formFragment.setTaskList(taskList);
                                if (i > 4 && i < size - 3) {
                                    formFragment.setGroupText(tenantAdapter.getItem(i - 5).getIdClient());
                                    formFragment.setTenant(tenantAdapter.getItem(i - 5));
                                }
                            }
                            textPageTextView.setText("Page " + (currentPos + 1) + " of " + formFragments.size());
                            totalTenantTextView.setText("" + tenantAdapter.getCount());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            serviceHandler.makeServiceCallAsyncTask(URL.TENANT_LIST, ServiceHandler.GET);
        }
        tenantAdapter.add(tenantList);
        int size = 8 + tenantAdapter.getCount();
        FormFragment[] tenantFragment = new FormFragment[tenantAdapter.getCount()];
        for (int i = 0; i < tenantFragment.length; i++) {
            tenantFragment[i] = new FormFragment();
            tenantFragment[i].setTenantCount(tenantAdapter.getCount());
            tenantFragment[i].setIdPm(taskList.getIdPm());
            tenantFragment[i].setDate(date);
            tenantFragment[i].setIdLayout(i + 5);
            tenantFragment[i].setTenant(tenantList.get(i));
            tenantFragment[i].setTaskList(taskList);
        }
        formFragments.addAll(5, Arrays.asList(tenantFragment));
        for (int i = 0; i < size; i++) {
            FormFragment formFragment = formFragments.get(i);
            formFragment.setTenantCount(tenantAdapter.getCount());
            formFragment.setIdPm(taskList.getIdPm());
            formFragment.setDate(date);
            formFragment.setIdLayout(i);
            formFragment.setTaskList(taskList);
            if (i > 4 && i < size - 3) {
                formFragment.setGroupText(tenantAdapter.getItem(i - 5).getIdClient());
                formFragment.setTenant(tenantAdapter.getItem(i - 5));
            }
        }
        textPageTextView.setText("Page " + (currentPos + 1) + " of " + formFragments.size());
        totalTenantTextView.setText("" + tenantAdapter.getCount());
    }

    private void addViewTenant() {
        linear = new LinearLayout(c);
        linear.setOrientation(LinearLayout.VERTICAL);
        if (tenantList == null) {
            getTenant();
        } else {
            for (int i = 0; i < tenantAdapter.getCount(); i++) {
                Tenant tenant = tenantAdapter.getItem(i);
                View view = LayoutInflater.from(c).inflate(R.layout.tenanttext, new LinearLayout(c), false);
                ((TextView) view.findViewById(R.id.textView_no)).setText("" + (i + 1));
                ((TextView) view.findViewById(R.id.textView_tenant)).setText(tenant.getIdClient());
                ((TextView) view.findViewById(R.id.textView_rfiDate)).setText(tenant.getRfiDate());
                ((TextView) view.findViewById(R.id.textView_equipmentType)).setText("" + (tenant.getEquipmentType() == 1 ? "Indoor" : "Outdoor"));
                ((TextView) view.findViewById(R.id.textView_powerSource)).setText(tenant.getSourcePower());
                linear.addView(view, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            }
        }
        linearLayout.addView(linear, 1, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    public void scrollTo(final View view) {
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.smoothScrollTo(0, view.getBottom());
            }
        });
    }

    public void removeViewTenant() {
        linearLayout.removeView(linear);
    }

    @Override
    public void setTitle(String title) {

    }

    private class TenantAdapter extends BaseAdapter<Tenant> {

        public TenantAdapter(Context context) {
            super(context);
        }

        @Override
        public View getView(int index, View convertView, ViewGroup parent) {
            return null;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        }
    }

}
