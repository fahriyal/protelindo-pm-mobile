package id.co.firzil.protelindopmmobile.ui;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import java.io.File;
import java.io.Serializable;

import id.co.firzil.protelindopmmobile.A;
import id.co.firzil.protelindopmmobile.Constants;
import id.co.firzil.protelindopmmobile.Delivery;
import id.co.firzil.protelindopmmobile.GPSUtils;
import id.co.firzil.protelindopmmobile.R;
import id.co.firzil.protelindopmmobile.Tag;
import id.co.firzil.protelindopmmobile.URL;
import id.co.firzil.protelindopmmobile.Utils;
import id.co.firzil.protelindopmmobile.entity.Meta;
import id.co.firzil.protelindopmmobile.fragment.BaseActivity;

/**
 * Created by Rio Rizky Rainey on 02/03/2015.
 * rizkyrainey@gmail.com
 */
public class UploadButton extends LinearLayout implements Serializable, View.OnClickListener {

    private String imagePath = "", tempPath="";

    public UploadButton(Context context) {
        super(context);
        init();
    }

    public UploadButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.sample_upload_button, this);
        setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(BaseActivity.getGpsTime) {
            A.get().setButton(this);

            if (TextUtils.isEmpty(imagePath)) {
                startCamera();
            } else {
                String location = imagePath;
                if (location.contains(Constants.FILE_IMAGE)) {
                    location = "file://" + location;
                } else {
                    location = URL.URL_IMAGE_META + location;
                }
                final ZoomImageDialog preview = new ZoomImageDialog((Activity) getContext(), location);
                preview.setEditButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startCamera();
                        preview.dismiss();
                    }
                });
                preview.show();
            }
        }
        else GPSUtils.showDialogUnknownLocation(view.getContext());
    }

    private void startCamera(){
        long time = BaseActivity.getGpsTime();
        A.start_camera = true;

        String fileName = Utils.getNewFilePath(time);
        Intent pickPhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        tempPath = fileName;

        pickPhoto.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(fileName)));
        ((Activity) getContext()).startActivityForResult(pickPhoto, Tag.REQUEST_CODE_CAMERA);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public synchronized void setImageTrulyPath(String newPath) {
        Meta meta = Meta.findByPathImage(imagePath);
        imagePath = newPath;
        if (meta != null) {
            meta.setMetaValue(imagePath);
            meta.setIsUpload(Delivery.PENDING); //sebelumnya gak ada
            meta.saveMerge();   //sebelumnya meta.update();
        }

        View tb = findViewById(R.id.linearUpload);
        if(! TextUtils.isEmpty(newPath)) {
            StateListDrawable states = new StateListDrawable();
            states.addState(new int[]{android.R.attr.state_pressed}, getResources().getDrawable(R.drawable.blue_pressed));
            states.addState(new int[]{android.R.attr.state_focused}, getResources().getDrawable(R.drawable.blue_focused));
            states.addState(new int[]{}, getResources().getDrawable(R.drawable.blue_normal));

            if(Utils.isUnder16()) tb.setBackgroundDrawable(states);
            else tb.setBackground(states);
        }

        tempPath = "";
    }

    public String getTempPath(){
        return tempPath;
    }

    public String getImagePath(){
        return imagePath;
    }

}
