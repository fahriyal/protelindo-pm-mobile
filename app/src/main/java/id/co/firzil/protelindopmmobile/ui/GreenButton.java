package id.co.firzil.protelindopmmobile.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.Button;

import id.co.firzil.protelindopmmobile.R;


public class GreenButton extends Button {

    private float widthDrawable;
    private float heightDrawable;

    private Drawable drawable;

    public GreenButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public GreenButton(Context context) {
        super(context);
        init(null, 0);
    }

    public GreenButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs, defStyleAttr);
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (drawable != null) {
            drawable.setBounds(0, 0, (int) widthDrawable, (int) heightDrawable); // ini ngerisize area Draw() drawablenya juga ukurannya.
//            drawable.draw(canvas);
            setCompoundDrawables(drawable, null, null, null); // set posisi drawable di sebelah kiri
        }
    }

    @SuppressWarnings("ResourceType")
    public void init(AttributeSet attrs, int defStyle) {
        setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.button_green_form));
        setTextColor(getContext().getResources().getColor(android.R.color.white));
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.GreenButton);
            heightDrawable = a.getDimension(0, (float) getHeight());
            widthDrawable = a.getDimension(1, (float) getWidth());
            drawable = a.getDrawable(R.styleable.GreenButton_drawable);
            a.recycle();
        }
    }

    public synchronized void setHeightDrawable(float heightDrawable) {
        this.heightDrawable = heightDrawable;
        postInvalidate();
    }

    public synchronized void setWidthDrawable(float widthDrawable) {
        this.widthDrawable = widthDrawable;
        postInvalidate();
    }

    public synchronized void setDrawable(Drawable drawable) {
        this.drawable = drawable;
        postInvalidate();
    }
}
