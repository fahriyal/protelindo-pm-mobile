package id.co.firzil.protelindopmmobile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by Rio Rizky Rainey on 01/07/2015.
 * rizkyrainey@gmail.com
 */
public class UrlServerActivity extends ActionBarActivity implements View.OnClickListener {

    private EditText urlText;
    private Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_secret_menu);
        urlText = (EditText) findViewById(R.id.editText_url);
        saveButton = (Button) findViewById(R.id.button_save);
        saveButton.setOnClickListener(this);
        urlText.setText(URL.URL_SERVER);
    }

    @Override
    public void onClick(View v) {
        String url = urlText.getText().toString();
        URL.URL_SERVER = url;
        getSharedPreferences("id.co.firzil.protelindopmmobile", MODE_PRIVATE).edit().putString("URL_SERVER", url).commit();
        URL.changeAllURL();
        Utils.makeToast(this, "Server sudah diganti");
        startActivity(new Intent(this, SplashActivity.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
